﻿using UnityEditor;
using UnityEngine;

namespace Tobe.Core.Editor
{
    [CustomEditor(typeof(MountableItemSlot), true)]
    public class WeaponEditor : Editor<MountableItemSlot>
    {
        private Color color = new Color(1, 0, 0, 0.1f);

        private void OnSceneGUI()
        {
            var itemPrefab =
                serializedObject.FindProperty(MountableItemSlot.ItemPrefabParamName).objectReferenceValue as Weapon;

            if (!itemPrefab) return;

            Handles.color = color;

            var pos = Target.transform.position;

            pos.Scale(new Vector3(1, 0, 1));

            Handles.DrawSolidDisc(pos, Vector3.up, itemPrefab.Range);

            Handles.Label(pos, "Range");
        }
    }
}