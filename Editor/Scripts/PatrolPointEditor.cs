﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Tobe.Core.Editor
{
    [CustomEditor(typeof(PatrolPoint)), CanEditMultipleObjects]
    public class PatrolPointEditor : Editor<PatrolPoint>
    {
        private bool _drawRanges;

        private IEnumerable<PatrolPoint> _patrolPoints;

        public override void OnInspectorGUI()
        {
            _drawRanges = EditorGUILayout.Toggle("Show brain ranges", _drawRanges);

            _patrolPoints = targets.OfType<PatrolPoint>();
            base.OnInspectorGUI();
        }

        private void OnSceneGUI()
        {
            if (_patrolPoints == null) return;

            foreach (var patrolPoint in _patrolPoints)
            {
                var range = patrolPoint.Range;

                if (_drawRanges)
                {
                    DrawRange(patrolPoint.transform.position, new Color(0, 1, 0, .1f), (float) range,
                        "Patrol successfull radius");
                }
            }
        }

        private void DrawRange(Vector3 point, Color drawColor, float radius, string label)
        {
            Handles.color = drawColor;

            var pos = point;

            pos.Scale(new Vector3(1, 0, 1));

            Handles.DrawSolidDisc(pos, Vector3.up, radius);

            var labelPos = pos + Vector3.forward * radius / 2f;

            Handles.Label(labelPos, label);
        }
    }
}