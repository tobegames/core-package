﻿using System;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace Tobe.Core.Editor
{
    [CustomEditor(typeof(TransformRandomizator))]
    public class RandomizatorEditor : OdinEditor
    {
        public void OnSceneGUI()
        {
            var t = target as TransformRandomizator;
            if(!t.randomizePosition) return;
            Handles.color = Color.green;
            Handles.DrawWireDisc(t.transform.position, t.transform.up,
                (t.minPositionRange)); // radius
            
            Handles.color = Color.red;
            Handles.DrawWireDisc(t.transform.position, t.transform.up,
                t.maxPositionRange); // radius
        }
    }

    public static class ArrayHelpers
    {
        public static ArrayContext<TArrayType> GetArray<TArrayType>(this SerializedObject context, string arrayPropName)
            where TArrayType : ScriptableObject
        {
            return new ArrayContext<TArrayType>(context, arrayPropName);
        }

        public static int GetIndex<T>(this T[] values, Func<T, bool> predicate)
        {
            for (var index = 0; index < values.Length; index++)
            {
                var value = values[index];
                if (predicate(value))
                {
                    return index;
                }
            }

            return -1;
        }
    }
}