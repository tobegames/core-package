﻿using System;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace Tobe.Core.Editor
{
    public abstract class Editor<TType> : OdinEditor where TType : class
    {
        protected TType Target { get; private set; }

        protected override void OnEnable()
        {
            Target = target as TType;
        }
    }
}