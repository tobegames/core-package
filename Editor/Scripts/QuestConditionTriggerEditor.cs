﻿using System.Linq;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace Tobe.Core.Editor
{
    [CustomEditor(typeof(QuestConditionTrigger))]
    public class QuestConditionTriggerEditor : OdinEditor
    {
        private GUIStyle _style;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (_style == null)
            {
                _style = new GUIStyle(EditorStyles.miniButtonLeft) {fixedWidth = 200};
            }

            var t = target as QuestConditionTrigger;


            var conditionsResolved = t.conditionsResolved;

            foreach (var tLinkedQuest in t.linkedQuests)
            {
                SirenixEditorGUI.Title(tLinkedQuest.title, "Conditions", TextAlignment.Left, true);

                foreach (var successCondition in tLinkedQuest.successConditions)
                {
                    GUILayout.BeginHorizontal();
                    var alreadyIn = conditionsResolved.FirstOrDefault(x =>
                        x.questId == tLinkedQuest.id && x.conditionId == successCondition.triggerId);

                    GUI.backgroundColor = alreadyIn != null ? Color.green : Color.gray;


                    if (GUILayout.Button(successCondition.title, _style))
                    {
                        if (alreadyIn == null)
                        {
                            t.conditionsResolved = conditionsResolved.Append(new QuestConditionPair
                            {
                                conditionId = successCondition.triggerId,
                                questId = tLinkedQuest.id
                            }).ToArray();
                        }
                        else
                        {
                        
                            t.conditionsResolved = conditionsResolved.Where(x => x != alreadyIn).ToArray();
                        }
                        serializedObject.ApplyModifiedProperties();
                    }

                    if (alreadyIn != null)
                    {
                        var value = SirenixEditorFields.IntField("Add Amount", alreadyIn.addAmount);
                        if (value != alreadyIn.addAmount)
                        {
                            alreadyIn.addAmount = value;
                            serializedObject.ApplyModifiedProperties();
                        }
                    }
                
                    GUILayout.EndHorizontal();
                }
            }
        }
    }
}