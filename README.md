# TOBE GAMES STUDIO Unity package

## Installation

1. Install manually dependencies:

 * [Extenject (Zenject)](https://assetstore.unity.com/packages/tools/utilities/extenject-dependency-injection-ioc-157735)

 * [JSON.NET](https://assetstore.unity.com/packages/tools/input-management/json-net-for-unity-11347)

 * Text Mesh PRO essentials
