﻿using Zenject;

public class TimerMonoInstaller : MonoInstaller<TimerMonoInstaller>
{
    public override void InstallBindings()
    {
        TimerInstaller.Install(Container);
    }
}