﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Tobe.Core
{
    public static class GameObjectHelpers
    {
        public static void ClearChildren(this Transform transform, bool destroyImmediate = false)
        {
            var count = transform.childCount;

            for (int i = 0; i < count; i++)
            {
                var child = transform.GetChild(i);

                if (destroyImmediate)
                {
                    Object.DestroyImmediate(child.gameObject);
                }
                else
                {
                    Object.Destroy(child.gameObject);
                }
            }
        }


        public static IEnumerable<TValue> GetComponentsInterfaced<TValue>(this GameObject component)
        {
            return !component ? Enumerable.Empty<TValue>() : component.GetComponents<MonoBehaviour>().OfType<TValue>();
        }

        public static IEnumerable<TValue> GetComponentsInterfaced<TValue>(this Component component)
        {
            return !component ? Enumerable.Empty<TValue>() : component.GetComponents<MonoBehaviour>().OfType<TValue>();
        }
    }
}