﻿using UnityEngine;
using Random = System.Random;

namespace Tobe.Core
{
    public static class VectorHelpers
    {
        private static Random _random;

        static VectorHelpers()
        {
            _random = new Random();
        }

        public static Vector3 RandomRange(Vector3 min, Vector3 max)
        {
            var x = _random.Next((int) (min.x * 100), (int) (max.x * 100)) / 100f;
            var y = _random.Next((int) (min.y * 100), (int) (max.y * 100)) / 100f;
            var z = _random.Next((int) (min.z * 100), (int) (max.z * 100)) / 100f;

            return new Vector3(x, y, z);
        }

        public static Vector3 Clamp(this Vector3 source, Vector3 min, Vector3 max)
        {
            return new Vector3(
                Mathf.Clamp(source.x, min.x, max.x),
                Mathf.Clamp(source.y, min.y, max.y),
                Mathf.Clamp(source.z, min.z, max.z)
            );
        }
    }
}