﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;
using Random = System.Random;

namespace Tobe.Core
{
    public interface IRandomizable
    {
        void Randomize();
    }

    public class TransformCache
    {
        public Vector3 LocalPosition { get; set; }
        public Quaternion LocalRotation { get; set; }
    }

    public class TransformRandomizator : MonoBehaviour, IRandomizable, IInitializable
    {
        private bool _inititalized;

        public bool ensureNotSameAsLast;

        public bool randomizeOnEnable;

        [SerializeField] private bool explicitItems;

        [SerializeField, ShowIf(nameof(explicitItems))]
        private GameObject[] objects;

        private Random _random;

        public bool randomizeRotation;

        [ShowIf(nameof(randomizeRotation))] [Tooltip("Deg")]
        public Vector3 minRotationRange;

        [ShowIf(nameof(randomizeRotation))] [Tooltip("Deg")]
        public Vector3 maxRotationRange = new Vector3(360, 360, 360);

        [ShowIf(nameof(randomizeRotation))] public bool isRotationAdditive;


        public bool randomizePosition;

        [ShowIf(nameof(randomizePosition))] public float minPositionRange;
        [ShowIf(nameof(randomizePosition))] public float maxPositionRange = 1;
        [ShowIf(nameof(randomizePosition))] public bool isPositionAdditive;

        private Dictionary<GameObject, TransformCache> _transforms;

        private void OnEnable()
        {
            Initialize();
            if (randomizeOnEnable)
            {
                Randomize();
            }
        }

        private int _lastIndex;


        [Button]
        public void Randomize()
        {
            Initialize();

            var index = _random.Next(0, objects.Length);

            if (ensureNotSameAsLast && objects.Length > 1)
            {
                if (_lastIndex == index)
                {
                    index = _lastIndex == 0 ? 1 : 0;
                }
            }

            _lastIndex = index;

            for (var i = 0; i < objects.Length; i++)
            {
                var o = objects[i];

                o.SetActive(index == i);

                if (index == i)
                {
                    if (randomizePosition)
                    {
                        var transf = _transforms[o];

                        var randomRange = QuaternionHelpers.RandomRange(Vector3.zero, new Vector3(0,359,0));
                        var randomPosition =
                            randomRange * transform.forward *
                            UnityEngine.Random.Range(minPositionRange, maxPositionRange);

                        if (isPositionAdditive)
                        {
                            o.transform.localPosition =
                                (o.transform.localPosition +
                                 randomPosition);
                        }
                        else
                        {
                            o.transform.localPosition = randomPosition
                                ;
                        }
                    }

                    if (randomizeRotation)
                    {
                        var transf = _transforms[o];

                        var randomRotation = QuaternionHelpers.RandomRange(minRotationRange, maxRotationRange);

                        if (isPositionAdditive)
                        {
                            o.transform.localRotation =
                                (o.transform.localRotation * randomRotation);
                        }
                        else
                        {
                            o.transform.localRotation =
                                transf.LocalRotation * randomRotation;
                        }
                    }
                }
            }
        }

        public void Initialize()
        {
            if (_inititalized) return;

            _inititalized = true;
            _transforms = new Dictionary<GameObject, TransformCache>();

            _random = new Random((int) DateTime.UtcNow.Ticks);

            if (!explicitItems)
            {
                objects = new GameObject[transform.childCount];

                for (int i = 0; i < objects.Length; i++)
                {
                    objects[i] = transform.GetChild(i).gameObject;
                }
            }

            foreach (var o in objects)
            {
                _transforms.Add(o, new TransformCache
                {
                    LocalPosition = o.transform.localPosition,
                    LocalRotation = o.transform.localRotation
                });
            }
        }
    }
}