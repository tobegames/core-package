﻿using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

public class AchievementServiceProxy : MonoBehaviour
{
    [SerializeField] private Achievement achievement;

    private IAchievementService _achievementService;

    [Inject]
    private void Ctor(IAchievementService achievementService)
    {
        _achievementService = achievementService;
    }

    [Button]
    public void GiveAchievement()
    {
        _achievementService.GiveAchievement(achievement);
    }

    [Button]
    public void PurgeAchievements()
    {
#if UNITY_EDITOR
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
#endif
    }
}