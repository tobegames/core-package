﻿public interface IPlayerAchievementRepository
{
    PlayerAchievementData[] PlayerAchievements { get; set; }
    void SaveAchievements();
}