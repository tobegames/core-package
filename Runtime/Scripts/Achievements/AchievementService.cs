﻿using System;
using System.Linq;
using UniRx;


public class AchievementService : IAchievementService, IDisposable
{
    private readonly IPlayerAchievementRepository _playerPrefsService;

    private readonly IAchievementRepository _achievementRepository;
    private readonly Subject<Achievement> _achievementGained;

    public IObservable<Achievement> AchievementGained => _achievementGained;

    private IInteractionUiManager _interactionUiManager;

    public AchievementService(
        IPlayerAchievementRepository playerPrefsService,
        IAchievementRepository achievementRepository, IInteractionUiManager interactionUiManager)
    {
        _playerPrefsService = playerPrefsService;
        _achievementRepository = achievementRepository;
        _interactionUiManager = interactionUiManager;

        _achievementGained = new Subject<Achievement>();
    }

    public void GiveAchievement(int id)
    {
        var playerAchievementData = _playerPrefsService.PlayerAchievements;

        if (playerAchievementData.Any(x => x.Id == id))
            return;

        var achievement = _achievementRepository.Achievements.FirstOrDefault(x => x.id.Equals(id));

        _playerPrefsService.PlayerAchievements =
            playerAchievementData.Append(new PlayerAchievementData {Id = achievement.id}).ToArray();

        _achievementGained.OnNext(achievement);

        _interactionUiManager.AchievementWindow.BindViewModel(new ConfirmableWindowViewModel
        {
            Label = "Achievement " + achievement.text,
            IsVisible = true,
            IsDirty = true,
            OnConfirm = () => { _interactionUiManager.AchievementWindow.Deactivate(); }
        });
        
        _interactionUiManager.AchievementWindow.Activate();
    }

    public void GiveAchievement(Achievement achievement)
    {
        GiveAchievement(achievement.id);
    }

    public void Dispose()
    {
        _achievementGained?.Dispose();
    }
}