﻿using UnityEngine;

[CreateAssetMenu(fileName = "Achievement", menuName = "Player/Achievements")]
public class Achievement : ScriptableObject
{
    public int id;
    public string text;
}