﻿using UnityEngine;

namespace Tobe.Core
{
    public interface IInteractionService
    {
        Collider GetScreenToWorldObject(LayerMask mask, float distance);
    }
}