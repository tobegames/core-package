﻿public class SingleUseTimer : Timer
{
    public SingleUseTimer(float period, bool isTimeScaled = true) : base(period, isTimeScaled)
    {
    }

    protected override void UpdateInternal()
    {
        if (CurrentTime >= Period)
        {
            Interval.OnNext(this);
            
            Dispose();
        }
    }
}

