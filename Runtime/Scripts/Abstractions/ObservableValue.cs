﻿using System;

namespace Tobe.Core.Abstractions
{
    public class ObservableValue<TValue> : IObservableValue<TValue>
    {
        private TValue _value;

        private readonly Action _onDispose;

        private ObservableValueEventHandler<TValue> _handler;

        public TValue Value
        {
            get => _value;
            set
            {
                if (!_value.Equals(value))
                {
                    _handler.oldValue = _value;

                    _handler.newValue = value;

                    OnChanged(_handler);
                }

                _value = value;
            }
        }

        public ObservableValue(TValue value, Action onDispose = null)
        {
            _value = value;
            _onDispose = onDispose;
        }

        public void Dispose()
        {
            Changed = null;
            _onDispose?.Invoke();
        }


        public event EventHandler<ObservableValueEventHandler<TValue>> Changed;

        public void SetSilent(TValue value)
        {
            _value = value;
        }

        protected virtual void OnChanged(ObservableValueEventHandler<TValue> e)
        {
            Changed?.Invoke(this, e);
        }
    }
}