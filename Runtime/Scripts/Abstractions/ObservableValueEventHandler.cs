﻿namespace Tobe.Core.Abstractions
{
    public struct ObservableValueEventHandler<TValue>
    {
        public TValue newValue;
        public TValue oldValue;
    }
}