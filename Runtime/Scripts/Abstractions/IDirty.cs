﻿namespace Tobe.Core.Abstractions
{
    public interface IDirty
    {
        bool IsDirty { get; }
        void CleanUp();
    }
}