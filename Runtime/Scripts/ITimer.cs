﻿using System;
using UnityEngine;
using UniRx;
using Zenject;

public interface ITimer : IDisposable, IObservable<ITimer>
{
    void Pause();

    void Resume();

    bool IsDisposed { get; }

    void Reset();

    float Period { get; }

    float CurrentTime { get; }

    IObservable<float> Tick { get; }

    void SetPeriod(float value);

    float GetPeriodPercentage();
}

public class TimerInstaller : Installer<TimerInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<TimerManager>().FromNewComponentOnNewGameObject()
            .WithGameObjectName("Timer Manager").AsSingle();
    }
}

public interface ITimerManager : IDisposable
{
    SingleUseTimer CreateSingleUseScaledTimer(float period);

    SingleUseTimer CreateSingleUseUnscaledTimer(float period);

    CyclicTimer CreateCyclicScaledTimer(float period, int cycles);

    CyclicTimer CreatePeriodicalUnscaledTimer(float period, int cycles);

    ReusableTimer CreateReusableScaledTimer(float period);

    ReusableTimer CreateReusableUnscaledTimer(float period);

    int TimersCount { get; }
}


[Serializable]
public abstract class Timer : ITimer
{
    private bool _doUpdate;
    private readonly Subject<float> _tick;

    public bool IsCountdown { get; set; }

    protected Subject<ITimer> Interval { get; }

    public bool IsDisposed { get; private set; }

    public void Reset()
    {
        CurrentTime = 0;
        OnReset();
    }

    protected virtual void OnReset()
    {
    }

    public float Period { get; private set; }

    public float CurrentTime { get; private set; }

    public IObservable<float> Tick => _tick;

    public void SetPeriod(float value)
    {
        if (value < 0)
        {
            throw new ArgumentOutOfRangeException();
        }

        Period = value;
    }

    public float GetPeriodPercentage()
    {
        if (Period <= 0) return 0;
        if (IsCountdown)
            return 1 - CurrentTime / Period;
        return CurrentTime / Period;
    }

    public bool IsTimeScaled { get; }

    protected Timer(float period, bool isTimeScaled = true)
    {
        Interval = new Subject<ITimer>();

        _tick = new Subject<float>();

        IsTimeScaled = isTimeScaled;

        Period = period;
    }

    public void Update()
    {
        if (!_doUpdate || IsDisposed) return;

        if (IsTimeScaled)
        {
            CurrentTime = Mathf.Clamp(CurrentTime + Time.deltaTime, 0, Period);
        }
        else
        {
            CurrentTime = Mathf.Clamp(CurrentTime + Time.unscaledDeltaTime, 0, Period);
        }

        if (IsCountdown)
        {
            _tick.OnNext(Period - CurrentTime);
        }
        else
        {
            _tick.OnNext(CurrentTime);
        }

        UpdateInternal();
    }

    protected abstract void UpdateInternal();

    public void Dispose()
    {
        if (IsDisposed) return;

        IsDisposed = true;

        Interval.OnCompleted();

        Interval.Dispose();

        _tick.Dispose();
    }


    public void Pause()
    {
        _doUpdate = false;
    }

    public void Resume()
    {
        _doUpdate = true;
    }

    public IDisposable Subscribe(IObserver<ITimer> observer)
    {
        return Interval.Subscribe(observer);
    }
}