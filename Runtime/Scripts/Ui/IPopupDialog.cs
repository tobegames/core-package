﻿using System;

namespace Tobe.Core
{
    public interface IPopupDialog : IDisposable
    {
        void Open();

        void Close();
    }
}