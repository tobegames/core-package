﻿using UnityEngine;
using UnityEngine.UI;

namespace Tobe.Core
{
    [RequireComponent(typeof(Button))]
    public class TobeIconButton : MonoBehaviour
    {
        [SerializeField] private Image icon;
    }
}