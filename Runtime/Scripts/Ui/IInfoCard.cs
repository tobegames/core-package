﻿namespace Tobe.Core
{
    public interface IInfoCard
    {
        void SetTitle(string value);

        string GetTitle();
    }
}