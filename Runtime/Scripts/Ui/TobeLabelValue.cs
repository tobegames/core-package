﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

public class TobeLabelValue : MonoBehaviour, ITobeLabelValue, IDisposable
{
    [SerializeField] private bool explicitLabel;

    [EnableIf(nameof(explicitLabel))] [SerializeField]
    private TobeLabel labelComponent;


    [EnableIf(nameof(explicitLabel))] [SerializeField]
    private TobeLabel valueComponent;

    public ITobeLabel Value { get; set; }

    public ITobeLabel Label { get; set; }


    [Inject]
    private void Ctor()
    {
        if (explicitLabel)
        {
            Label = labelComponent ? labelComponent : GetComponentsInChildren<TobeLabel>().FirstOrDefault();
            Value = valueComponent
                ? valueComponent
                : GetComponentsInChildren<TobeLabel>().FirstOrDefault(x => x as ITobeLabel != Label);
        }
        else
        {
            Label = GetComponentsInChildren<TobeLabel>().FirstOrDefault();
            Value = GetComponentsInChildren<TobeLabel>().FirstOrDefault(x => x as ITobeLabel != Label);
        }
    }
    

    public void SetValue(string value)
    {
        Value.SetText(value);
    }

    public string GetValue()
    {
        return Value.GetText();
    }

    public void SetLabel(string value)
    {
       
        Label.SetText(value);
    }

    public string GetLabel()
    {
        return Label.GetText();
    }

    public void Dispose()
    {
        labelComponent?.Dispose();
        valueComponent?.Dispose();
    }
}