﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TobeValueIcon : MonoBehaviour, ITobeValueIcon, IDisposable
{
    [SerializeField]
    private TobeLabel label;
    [SerializeField]
    private Image iconImage;

    public Sprite Icon
    {
        get => iconImage.sprite;
        set => iconImage.sprite = value;
    }
    
    public ITobeLabel Value
    {
        get => label;
        set => label = (TobeLabel) value;
    }

    public void Dispose()
    {
        label?.Dispose();
    }
}