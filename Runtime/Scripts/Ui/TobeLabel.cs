﻿using System;
using TMPro;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(TextMeshProUGUI))]
[ZenjectAllowDuringValidation]
public class TobeLabel : MonoBehaviour, ITobeLabel, IDisposable
{
    private TextMeshProUGUI _text;

    [Inject]
    private void Ctor()
    {
        _text = GetComponent<TextMeshProUGUI>();
    }

    public string GetText()
    {
        if(!_text)
            _text = GetComponent<TextMeshProUGUI>();

        return _text.text;
    }

    public void SetText(string value)
    {
        if(!_text)
            _text = GetComponent<TextMeshProUGUI>();

        _text.text = value;
    }

    public void Dispose()
    {
        _text = null;
    }
}