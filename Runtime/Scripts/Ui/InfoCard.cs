﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace Tobe.Core
{
    public class InfoCard : MonoBehaviour, IDisposable
    {
        [SerializeField] private InfoCardHeader header;
        [SerializeField] private InfoCardBody body;
        [SerializeField] private InfoCardFooter footer;

        public InfoCardHeader Header => header;

        public InfoCardBody Body => body;

        public InfoCardFooter Footer => footer;

        public UnityEvent onCloseCard;
        
        public BoolReactiveProperty IsVisible { get; private set; }

        private void Awake()
        {
            IsVisible = new BoolReactiveProperty();
        }

        public void Dispose()
        {
            IsVisible?.Dispose();
        }
    }
}