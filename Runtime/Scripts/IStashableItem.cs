﻿namespace Tobe.Core
{
    public interface IStashableItem : IInteractable
    {
        int Id { get; }

        void Stash();
    }

    public interface IStashManager
    {
    }
}