﻿using System;
using Tobe.Core.Mvc.Implementations;

namespace Tobe.Core.Mvc
{
    public interface ISimpleView : IView<SimpleViewModel>
    {
    }
}