﻿namespace Tobe.Core.Mvc
{
    public interface ISimpleModel
    {
        bool IsVisible { get; set; }
    }
}