﻿using System;
using UnityEngine;
using Zenject;

namespace Tobe.Core.Mvc.Implementations
{
    [ZenjectAllowDuringValidation]
    public abstract class MonoControllerProxy<TView, TController> : MonoBehaviour, IValidatable
        where TController : class, IController<TView>
    {
        protected abstract TView View { get; }

        protected TController Controller;

        public void Dispose()
        {
            Controller.Dispose();
        }
        
        private void Start()
        {
            OnStarted();
        }


        protected virtual void OnStarted()
        {
        }


        private void Update()
        {
            OnUpdate();
        }

        protected virtual void OnUpdate()
        {
        }

        [Inject]
        private void Ctor(TController controller)
        {
            if (controller == this as TController)
            {
                throw new StackOverflowException("Don't bind '" + nameof(TController) +
                                                 " to self! Implement IController<" + nameof(TView) +
                                                 "> and bind it instead.");
            }

            Controller = controller;

            BindView(View);
        }

        public void BindView(TView model)
        {
            Controller?.BindView(View);
        }

        public void Validate()
        {
            if (typeof(TController) == GetType())
            {
                // Common mistake that controller generic param in controller is not IController but instead implementation
                
                throw new StackOverflowException("Don't bind '" + nameof(TController) +
                                                 " to self! Implement IController<" + nameof(TView) +
                                                 "> and bind it instead.");
            }
        }
    }
}