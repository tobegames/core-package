﻿using UnityEngine;

namespace Tobe.Core.Mvc.Implementations
{
    public class SimpleControllerProxy :
        MonoViewControllerProxy<SimpleViewModel, ISimpleView, ISimpleController>, ISimpleController
    {
        [SerializeField] private SimpleView view;

        protected override ISimpleView View => view;

        public void SetVisible(bool value)
        {
            Controller.SetVisible(value);
        }

        public void ToggleVisible()
        {
            Controller.ToggleVisible();
        }

        public void ToggleActive()
        {
            Controller.ToggleActive();
        }

        public void SetActive(bool value)
        {
            Controller.SetActive(value);
        }

        public void BindViewModel(SimpleViewModel model)
        {
            Controller.BindViewModel(model);
        }
    }
}