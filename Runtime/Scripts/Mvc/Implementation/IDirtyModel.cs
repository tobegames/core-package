﻿namespace Tobe.Core.Mvc.Implementations
{
    public interface IDirtyModel
    {
        bool IsDirty { get; set; }
    }
}