﻿using System;
using System.Collections;
using UnityEngine;

namespace Tobe.Core.Mvc.Implementations
{
    public class FuncCoroutinePlaceholder : ICoroutineLogicPlaceHolder
    {
        public IEnumerator Logic
        {
            get { yield return _logic(); }
        }

        private readonly Func<YieldInstruction> _logic;

        public FuncCoroutinePlaceholder(Func<YieldInstruction> logic)
        {
            _logic = logic;
        }

        public void Dispose()
        {
        }
    }
}