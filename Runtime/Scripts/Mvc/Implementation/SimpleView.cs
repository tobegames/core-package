﻿using System;
using UnityEngine;

namespace Tobe.Core.Mvc.Implementations
{
    [Serializable]
    public class SimpleView : ISimpleView
    {
        [SerializeField]
        private MonoRenderer renderer;

        [SerializeField] private GameObject root;

        public void RenderModel(SimpleViewModel model)
        {
            renderer.SetVisible(model.isVisible);
            root.SetActive(model.isActive);
        }
    }
}
