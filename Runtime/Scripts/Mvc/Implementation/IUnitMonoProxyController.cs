﻿using Tobe.Core.Mvc;
using UnityEngine;

public interface IDialogTarget : IInteractable
{
    IUnitIdentifier UnitIdentifier { get; }

    IUnitDisplayDataProvider DisplayDataProvider { get; }

    IUnitDialogProvider DialogProvider { get; }

    bool TryBeginDialog(IUnitIdentifier initiator, Vector3 initiatorWorldPosition);
}