﻿using System;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Tobe.Core.Mvc.Implementations
{
    [ZenjectAllowDuringValidation]
    public abstract class
        MonoViewControllerProxy<TViewModel, TView, TController> : MonoControllerProxy<TView, TController>
        where TController : class, IController<TView, TViewModel>
        where TViewModel : class, IDirtyModel
        where TView : class, IView<TViewModel>
    {
        [SerializeField] protected ViewModelUpdateStrategy updateStrategy;

        [SerializeField] protected TViewModel viewModel;


        [Inject]
        private void Ctor()
        {
            BindViewModel(viewModel);
        }

        protected override void OnStarted()
        {
            CleanUp();
        }

        protected override void OnUpdate()
        {
            if (updateStrategy != ViewModelUpdateStrategy.OnDemand)
            {
                if (updateStrategy == ViewModelUpdateStrategy.OnUpdate ||
                    Controller.IsDirty && updateStrategy == ViewModelUpdateStrategy.WhenDirty)
                {
                    CleanUp();
                }
            }
        }

        public void BindViewModel(TViewModel model)
        {
            Controller?.BindViewModel(model);
        }

        public bool IsDirty => Controller.IsDirty;

        public void CleanUp()
        {
            Controller.CleanUp();
        }
    }
}