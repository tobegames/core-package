﻿using System;
using System.Collections;
using UnityEngine;

namespace Tobe.Core.Mvc.Implementations
{
    public class CoroutineCompletion : ICoroutineLogicPlaceHolder
    {
        private readonly Action _onComplete;

        public CoroutineCompletion(Action onComplete)
        {
            _onComplete = onComplete;
        }

        public void Dispose()
        {
            _isDisposed = true;
        }

        private bool _isDisposed;

        public IEnumerator Logic
        {
            get
            {
                if (_isDisposed) yield return null;

                _onComplete?.Invoke();
                
                yield return null;
            }
        }
    }
}