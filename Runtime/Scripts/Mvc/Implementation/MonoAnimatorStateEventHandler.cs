﻿using UnityEngine;

namespace Tobe.Core.Mvc.Implementations
{
    public struct MonoAnimatorStateEventHandler
    {
        public AnimatorStateInfo AnimatorStateInfo { get; set; }

        public int LayerIndex { get; set; }
    }
}