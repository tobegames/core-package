﻿public class DamageSignal
{
    public IDamageSource DamageSource { get; set; }
    public int DamageAmount { get; set; }
}


