﻿using System;
using System.Collections;
using UnityEngine;

namespace Tobe.Core.Mvc.Implementations
{
    public interface ICoroutineLogicPlaceHolder : IDisposable
    {
        IEnumerator Logic { get; }
    }
    
    public interface ICoroutineLogicPlaceHolder<in TContext> : ICoroutineLogicPlaceHolder where TContext : class
    {
        IEnumerator ProcessLogic(TContext context);
    }
}