﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using Tobe.Core.Mvc.Abstractions;
using UnityEngine.Events;

namespace Tobe.Core.Mvc.Implementations
{
    [Serializable]
    public class MonoComponentAnimationEvent : UnityEvent<MonoComponentAnimationEventArgs>
    {
    }

    [Serializable]
    public class MonoComponentAnimationEventArgs
    {
        public int intValue;
        public float floatValue;
        public string stringValue;
        public object objectValue;
    }

    [RequireComponent(typeof(Animator))]
    public class MonoAnimator : AViewComponent, IAnimatorComponent
    {
        private Animator _animator;

        private RuntimeAnimatorController _defaultController;

        public RuntimeAnimatorController RuntimeAnimator
        {
            get => _animator ? _animator.runtimeAnimatorController : null;
            set => _animator.runtimeAnimatorController = value;
        }

        protected override void OnAwoke()
        {
            _animator = GetComponent<Animator>();
            _defaultController = _animator.runtimeAnimatorController;
        }

        public MonoComponentAnimationEvent animationEventFired;

        public event EventHandler<MonoAnimatorStateEventHandler> StateExited;

        public event EventHandler<MonoAnimatorStateEventHandler> StateEntered;

        public override void OnDestroyed()
        {
            StopAllCoroutines();
        }

        public void SetTrigger(int id)
        {
            _animator.SetTrigger(id);
        }

        public void SetSpeed(float value)
        {
            _animator.speed = value;
        }

        public void ResetTrigger(int id)
        {
            _animator.ResetTrigger(id);
        }

        public void StopParameter(Coroutine coroutine)
        {
            if (coroutine == null) return;

            StopCoroutine(coroutine);
        }

        public Coroutine SetFloatParameterAnimated(int id,
            AnimationCurve curve,
            float speed = 1,
            float? axisXMaxValue = null)
        {
            if (!axisXMaxValue.HasValue)
            {
                axisXMaxValue = curve.keys.Max(x => x.time);
            }

            return StartCoroutine(ValueAnimation(id, speed, curve, axisXMaxValue.Value,
                SetParameter));
        }


        public Coroutine SetIntParameterAnimated(int id,
            AnimationCurve curve,
            float speed = 1,
            float? axisXMaxValue = null)
        {
            if (!axisXMaxValue.HasValue)
            {
                axisXMaxValue = curve.keys.Max(x => x.time);
            }

            return StartCoroutine(ValueAnimation(id, speed, curve, axisXMaxValue.Value,
                (index, value) => { SetParameter(index, (int) value); }));
        }

        public Coroutine SetLayerWeightAnimated(int id, AnimationCurve curve, float speed = 1,
            float? axisXMaxValue = null)
        {
            if (!axisXMaxValue.HasValue)
            {
                axisXMaxValue = curve.keys.Max(x => x.time);
            }

            return StartCoroutine(ValueAnimation(id, speed, curve, axisXMaxValue.Value, SetLayerWeight));
        }

        public void ResetController()
        {
            if (RuntimeAnimator)
                RuntimeAnimator = _defaultController;
        }

        private IEnumerator ValueAnimation(int paramId, float speed, AnimationCurve curve, float axisXMaxValue,
            Action<int, float> logic)
        {
            var progress = 0f;

            while (progress < axisXMaxValue)
            {
                if (progress >= axisXMaxValue)
                {
                    yield break;
                }

                var value = curve.Evaluate(progress);

                logic(paramId, value);

                SetParameter(paramId, value);

                yield return new WaitForEndOfFrame();

                progress += Time.deltaTime * speed;
            }
        }


        public void SetLayerWeight(int layerId, float value)
        {
            if (_animator)
                _animator.SetLayerWeight(layerId, value);
        }

        public void SetParameter(int id, float value)
        {
            if (_animator)
                _animator.SetFloat(id, value);
        }

        public void SetParameter(int id, bool value)
        {
            if (_animator)
                _animator.SetBool(id, value);
        }

        public void SetParameter(int id, int value)
        {
            if (_animator)
                _animator.SetInteger(id, value);
        }

        public bool GetBoolParameter(int id)
        {
            return _animator.GetBool(id);
        }

        public int GetIntParameter(int id)
        {
            return _animator.GetInteger(id);
        }

        public float GetFloatParameter(int id)
        {
            return _animator.GetFloat(id);
        }

        public void PlayState(string stateName)
        {
            if (_animator)
                _animator.Play(stateName);
        }

        public void PlayState(string stateName, int layerIndex)
        {
            if (_animator)
                _animator.Play(stateName, layerIndex);
        }

        public void PlayState(string stateName, string layerName)
        {
            if (_animator)
                PlayState(stateName, GetLayerIndex(layerName));
        }

        public void PlayState(int stateId, int layerIndex)
        {
            if (_animator)
                _animator.Play(stateId, layerIndex);
        }

        public int GetLayerIndex(string layerName)
        {
            return _animator.GetLayerIndex(layerName);
        }

        public float GetLayerWeight(int layerId)
        {
            return _animator.GetLayerWeight(layerId);
        }

        public string GetLayerName(int layerId)
        {
            return _animator.GetLayerName(layerId);
        }

        public void InvokeStateExited(AnimatorStateInfo stateInfo, int layerIndex)
        {
            StateExited?.Invoke(this, new MonoAnimatorStateEventHandler
            {
                LayerIndex = layerIndex,
                AnimatorStateInfo = stateInfo
            });
        }

        public void InvokeStateEntered(AnimatorStateInfo stateInfo, int layerIndex)
        {
            StateEntered?.Invoke(this, new MonoAnimatorStateEventHandler
            {
                LayerIndex = layerIndex,
                AnimatorStateInfo = stateInfo
            });
        }

        public void FireValueInt(int value)
        {
            animationEventFired?.Invoke(new MonoComponentAnimationEventArgs
            {
                intValue = value
            });
        }


        public void FireValueString(string value)
        {
            animationEventFired?.Invoke(new MonoComponentAnimationEventArgs
            {
                stringValue = value
            });
        }


        public void FireValueFloat(float value)
        {
            animationEventFired?.Invoke(new MonoComponentAnimationEventArgs
            {
                floatValue = value
            });
        }


        public void FireValueObject(object value)
        {
            animationEventFired?.Invoke(new MonoComponentAnimationEventArgs
            {
                objectValue = value
            });
        }

        public void SetPlaybackTime(float time)
        {
            if (_animator)
                _animator.playbackTime = time;
        }

    }
}