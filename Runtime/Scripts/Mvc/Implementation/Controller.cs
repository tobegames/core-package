﻿namespace Tobe.Core.Mvc.Implementations
{
    public class Controller<TView, TViewModel> : IController<TView, TViewModel> where TView : IView<TViewModel>
        where TViewModel : class, IDirtyModel
    {
        public TView View { get; private set; }

        protected TViewModel ViewModel { get; private set; }

        public virtual void BindView(TView view)
        {
            View = view;
        }

        public virtual void BindViewModel(TViewModel model)
        {
            ViewModel = model;
        }

        public void RenderViewModel()
        {
            View.RenderModel(ViewModel);
        }

        public void Dispose()
        {
            if (IsDisposed)
            {
                return;
            }

            DisposeInternal();

            IsDisposed = true;
        }

        protected virtual void DisposeInternal()
        {
        }

        protected bool IsDisposed { get; private set; }

        public bool IsDirty => ViewModel.IsDirty;
        
        public void CleanUp()
        {
            RenderViewModel();
            ViewModel.IsDirty = false;
        }
    }
}