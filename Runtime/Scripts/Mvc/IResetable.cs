﻿namespace Tobe.Core.Mvc
{
    public interface IResetable
    {
        void InstanceReset();
    }
}