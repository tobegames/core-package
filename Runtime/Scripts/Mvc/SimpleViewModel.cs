﻿using System;
using UnityEngine;
using Tobe.Core.Mvc.Implementations;

namespace Tobe.Core.Mvc
{
    [Serializable]
    public class SimpleViewModel : IDirtyModel
    {
        public bool isVisible = true;

        public bool isActive = true;

        public bool IsDirty { get; set; }
    }
}