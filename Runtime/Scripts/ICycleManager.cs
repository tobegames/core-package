﻿using System;

namespace Tobe.Core
{
    public interface ICycleManager : IDisposable
    {
        void EndCycle();

        void StartCycle();
        
        void EndCycle<TContext>(TContext context);

        void StartCycle<TContext>(TContext context);
        
        void UnRegister(ICycleHandler cycleHandler);
        
        void Register(ICycleHandler cycleHandler);
    }
}