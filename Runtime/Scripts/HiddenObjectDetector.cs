﻿using System;
using UnityEngine;

namespace Tobe.Core
{
    [RequireComponent(typeof(Camera))]
    public class HiddenObjectDetector : MonoBehaviour
    {
        private Camera _camera;

        [SerializeField] private Transform trackTo;

        private void Awake()
        {
            _camera = GetComponent<Camera>();
        }

        [SerializeField] private LayerMask layerMask;

        [SerializeField] private Material switchMaterial;
        private Material _lastMaterial;
        private Renderer _lastRenderer;

        private void Update()
        {
            var cameraPosition = _camera.transform.position;
            var trackToPosition = trackTo.position;
            var distance = Vector3.Distance(cameraPosition, trackToPosition);

            var direction = trackToPosition - cameraPosition;

            var ray = new Ray(cameraPosition, direction);


            if (Physics.Raycast(ray, out var hitInfo, distance, layerMask) && trackTo != hitInfo.transform)
            {
                var colliderGameObject = hitInfo.collider.gameObject;

                var delegated = colliderGameObject.GetComponent<DelegatedCollider>();
                if (delegated)
                    colliderGameObject = delegated.Parent;

                var rendererComponent = colliderGameObject.GetComponent<Renderer>();
                if (rendererComponent && _lastRenderer != rendererComponent)
                {
                    if (_lastRenderer)
                    {
                        var mats =  _lastRenderer.materials;
                        mats[0] = _lastMaterial;
                        _lastRenderer.materials = mats;
                    }

                    _lastRenderer = rendererComponent;
                    _lastMaterial = _lastRenderer.material;

                    var materials = _lastRenderer.materials;
                    materials[0] = switchMaterial;
                    _lastRenderer.materials = materials;

                }
                Debug.DrawLine(ray.origin, ray.GetPoint(distance), Color.green);
            }
            else
            {
                if (_lastRenderer)
                {
                    var mats = _lastRenderer.materials;
                    mats[0] = _lastMaterial;
                    _lastRenderer.materials = mats;
                    _lastRenderer = null;
                    _lastMaterial = null;
                }

                Debug.DrawLine(ray.origin, ray.GetPoint(distance), Color.red);
            }
        }
    }
}