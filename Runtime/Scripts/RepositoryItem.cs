﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Tobe.Core
{
    [Serializable]
    public class RepositoryItem
    {
        [PreviewField] public Component prefab;

        public IStashableItem StashableItem => prefab.GetComponent<IStashableItem>();
    }
}