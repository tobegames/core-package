﻿using Zenject;

public class TobeEssentialsMonoInstaller : MonoInstaller<TobeEssentialsMonoInstaller>
{
    public override void InstallBindings()
    {
        TobeEssentialsInstaller.Install(Container);
    }
}