﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;
using Object = UnityEngine.Object;

public enum UnityMethodParams
{
    Integer,
    Boolean,
    Str,
    Vec3,
    Obj
}

[Serializable]
public class UnityMethodInvoke
{
    public string methodName;

    public bool boolValue;

    public UnityMethodParams unityMethodParams;
    
    public Vector3 vector3;

    public int intValue;
    
    public string strValue;

    public Object objValue;
    
    public void Invoke(GameObject gameObject)
    {
        if(string.IsNullOrEmpty(methodName)) return;
        switch (unityMethodParams)
        {
            case UnityMethodParams.Integer:
                gameObject.SendMessage(methodName, intValue, SendMessageOptions.DontRequireReceiver);
                break;
            case UnityMethodParams.Boolean:
                gameObject.SendMessage(methodName, boolValue, SendMessageOptions.DontRequireReceiver);
                break;
            case UnityMethodParams.Str:
                gameObject.SendMessage(methodName,  strValue, SendMessageOptions.DontRequireReceiver);
                break;
            case UnityMethodParams.Vec3:
                gameObject.SendMessage(methodName,  vector3, SendMessageOptions.DontRequireReceiver);
                break;
            case UnityMethodParams.Obj:
                gameObject.SendMessage(methodName,  objValue, SendMessageOptions.DontRequireReceiver);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}

[Serializable]
public class MonoBehaviourTimelineMethodCallData : PlayableBehaviour
{

    public UnityMethodInvoke onPlayData;
    
    public UnityMethodInvoke onEndData;
    
    [HideInInspector] public GameObject gameObject;

    
    
    public MonoBehaviourTimelineMethodCallData()
    {
    }
    
    

    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        onPlayData.Invoke(gameObject);
    }

    public override void OnBehaviourPause(Playable playable, FrameData info)
    {
        onEndData.Invoke(gameObject);
    }
}