﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class MonoBehaviourTimelineMethodCallClip : PlayableAsset, ITimelineClipAsset
{
    public MonoBehaviourTimelineMethodCallData template = new MonoBehaviourTimelineMethodCallData();
    public override Playable CreatePlayable(PlayableGraph graph, GameObject go)
    {
        var test = ScriptPlayable<MonoBehaviourTimelineMethodCallData>.Create(graph,template);
        
        return test;
    }

    public ClipCaps clipCaps => ClipCaps.Blending;
}