﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

// The Clip
[System.Serializable]
public class DialogClip : PlayableAsset, ITimelineClipAsset
{
    public DialogContentBehaviour template = new DialogContentBehaviour();
//    public DialogManager dialogManager;

    // Factory method that generates a playable based on this asset
    public override Playable CreatePlayable(PlayableGraph graph, GameObject go)
    {
        var test = ScriptPlayable<DialogContentBehaviour>.Create(graph,template);
        return test;
    }

    public ClipCaps clipCaps => ClipCaps.Blending;
}
