﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimelineDialogManager : MonoBehaviour
{
    public TextMeshProUGUI npcNameLabel;
    public Image npcAvatar;
    public TextMeshProUGUI dialogText;
}
