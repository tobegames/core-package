﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackClipType(typeof(MonoBehaviourTimelineMethodCallClip))]
[TrackBindingType(typeof(GameObject))]
public class MonoBehaviourTimelineMethodCallTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        // before building, update the binding field in the clips assets;
        var director = go.GetComponent<PlayableDirector>();

        var binding = director.GetGenericBinding(this);

        foreach (var c in GetClips())
        {
            var myAsset = c.asset as MonoBehaviourTimelineMethodCallClip;
            if (myAsset != null)
                myAsset.template.gameObject = binding as GameObject;
        }

        return base.CreateTrackMixer(graph, go, inputCount);
    }
}