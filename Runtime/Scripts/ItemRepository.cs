﻿using System.Collections.Generic;
using System.Linq;
using ModestTree;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

namespace Tobe.Core
{
    [CreateAssetMenu(menuName = "Repositories/Item Repository")]
    public class ItemRepository : ScriptableObject, IItemRepository, IValidatable
    {
        public IReadOnlyDictionary<int, RepositoryItem> Items { get; private set; }

        [SerializeField] [ListDrawerSettings(Expanded = true)]
        private List<Component> mountableItems;


        public void Validate()
        {
            Assert.That(mountableItems.All(x => x.GetComponent<IStashableItem>() != null));

            var groups = mountableItems.GroupBy(x => x.GetComponent<IStashableItem>()).Where(x => x.Count() > 1);

            if (groups.Any())
            {
                foreach (var @group in groups)
                {
                    Debug.LogError("Duplicit id: " + group.Key.Id);
                    foreach (var keyValuePair in group)
                    {
                        Debug.LogError("Item id " + keyValuePair + " has duplicit Id");
                    }
                }
            }
            
            Items = mountableItems.Select(x => new RepositoryItem
                {
                    prefab = x
                })
                .ToDictionary(x => x.StashableItem.Id, y => y);

            Assert.That(Items.All(x => x.Value != null), "Items.All(x=>x.Value!= null)");
        }
    }
}