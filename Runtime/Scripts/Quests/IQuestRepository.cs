﻿using System.Collections.Generic;

public interface IQuestRepository
{
    IEnumerable<QuestData> AllQuests { get; }
}