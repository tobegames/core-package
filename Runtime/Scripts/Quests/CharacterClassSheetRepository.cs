﻿using System.Collections.Generic;
using UnityEngine;

public interface ICharacterClassSheetRepository
{
    IEnumerable<CharacterClassSheet> CharacterClassSheets { get; }
}

[CreateAssetMenu(menuName = "Repositories/Character Class Sheets")]
public class CharacterClassSheetRepository : ScriptableObject, ICharacterClassSheetRepository
{
    [SerializeField]
    private CharacterClassSheet[] characterClassSheets;
    public IEnumerable<CharacterClassSheet> CharacterClassSheets => characterClassSheets;
}

