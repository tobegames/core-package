﻿using ModestTree;
using Zenject;

public class DialogMonoInstaller : MonoInstaller<DialogMonoInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<DialogFunctionsContext>().FromNewComponentOnNewGameObject().WithGameObjectName("Dialog Function Context").AsSingle();
        
        DialogInstaller.Install(Container);
    }
}