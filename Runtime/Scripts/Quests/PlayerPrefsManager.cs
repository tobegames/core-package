﻿using System;
using Newtonsoft.Json;
using UnityEngine;

public class PlayerPrefsManager : IPlayerQuestsRepository, IPlayerAchievementRepository, IPlayerResourcesRepository,
    IDisposable
{
    private PlayerQuestData[] _playerQuestDatas;
    private PlayerAchievementData[] _playerAchievements;
    private PlayerResourceItem[] _playerResourceItems;
    
    private const string PlayerQuestData = "player.quest.data";
    private const string PlayerAchievementData = "player.achieve.data";
    private const string PlayerResourcesData = "player.resources.data";

    public PlayerAchievementData[] PlayerAchievements
    {
        get
        {
            if (_playerAchievements == null)
            {
                var jsonVal = PlayerPrefs.GetString(PlayerAchievementData, null);
                _playerAchievements = JsonConvert.DeserializeObject<PlayerAchievementData[]>(jsonVal) ??
                                      new PlayerAchievementData[0];
            }


            return _playerAchievements;
        }
        set => _playerAchievements = value;
    }


    public PlayerResourceItem[] Resources
    {
        get
        {
            if (_playerResourceItems == null)
            {
                var jsonVal = PlayerPrefs.GetString(PlayerResourcesData, null);
                _playerResourceItems = JsonConvert.DeserializeObject<PlayerResourceItem[]>(jsonVal) ??
                                       new PlayerResourceItem[0];
            }

            return _playerResourceItems;
        }
        set => _playerResourceItems = value;
    }


    public PlayerQuestData[] AllPlayerQuests
    {
        get
        {
            if (_playerQuestDatas == null)
            {
                var value = PlayerPrefs.GetString(PlayerQuestData, null);

                _playerQuestDatas =
                    JsonConvert.DeserializeObject<PlayerQuestData[]>(value) ??
                    new PlayerQuestData[0];
            }

            return _playerQuestDatas;
        }
        set => _playerQuestDatas = value;
    }

    public void SaveAchievements()
    {
        var jsonVal = JsonConvert.SerializeObject(_playerAchievements);
        PlayerPrefs.SetString(PlayerAchievementData, jsonVal);
    }

    public void SaveQuestData()
    {
        var jsonVal = JsonConvert.SerializeObject(_playerQuestDatas);
        PlayerPrefs.SetString(PlayerQuestData, jsonVal);
    }

    public void Dispose()
    {
        SaveAchievements();
        SaveQuestData();
        SaveResources();
    }

    public void SaveResources()
    {
        var jsonVal = JsonConvert.SerializeObject(_playerResourceItems);
        PlayerPrefs.SetString(PlayerResourcesData, jsonVal);
    }
}