﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

[Serializable]
public class EnemyContainerContext
{
    public int combatGroupId = 1;
    public string unitId = "new enemy";
    public float aggressiveRange = 3f;
   
    
    
    [Tooltip("This will be used to decide if unit is alive or dead")] [OnValueChanged(nameof(LifeStatChanged))]
    public UnitStatMetadata lifeStatMetadata;

    public FloatStat lifeStat;

    [InfoBox("Drag Stats repository here to edit character stats", InfoMessageType.Warning, nameof(ShowWarning))]
    [SerializeField] private StatsRepository statsRepository;

    [ValueDropdown(nameof(GetStats))] [LabelText("Select stat to add")] [SerializeField] [ShowIf(nameof(statsRepository))]
    private UnitStatMetadata selectedStat;

    [ListDrawerSettings(CustomAddFunction = nameof(AddStat)), EnableIf(nameof(EnableAdd))]
    public FloatStat[] stats;

    public string displayName;
    private bool EnableAdd => statsRepository && selectedStat;
    private bool ShowWarning => statsRepository == null;

    private FloatStat AddStat()
    {
        if (selectedStat != null && !string.IsNullOrEmpty(selectedStat.id))
        {
            return new FloatStat(selectedStat);
        }

        return null;
    }

    private IEnumerable<UnitStatMetadata> GetStats()
    {
        return statsRepository.allKnownStats;
    }

    private void LifeStatChanged()
    {
        lifeStat = !lifeStatMetadata ? null : new FloatStat(lifeStatMetadata);

        if (lifeStat != null)
            MonoBehaviour.print(lifeStat.Id);
    }
}

public class NpcMonoInstaller : MonoInstaller<NpcMonoInstaller>
{
    [SerializeField] private NpcContainerContext npcContainerContext;

    [SerializeField] private int combatGroupId;

    public override void InstallBindings()
    {
        Container.Bind<IUnitIdentifier>().To<NpcUnitIdentifier>()
            .FromInstance(new NpcUnitIdentifier(npcContainerContext.npcMetadata.npcId, combatGroupId))
            .AsSingle();

        Container.Bind<IUnitDisplayDataProvider>().To<NpcDisplayDataProvider>()
            .FromInstance(new NpcDisplayDataProvider(npcContainerContext.npcMetadata.displayName)).AsSingle();

        Container.Bind<IUnitDialogProvider>().To<NpcDialogProvider>()
            .FromInstance(new NpcDialogProvider(npcContainerContext.npcMetadata.dialogLines)).AsSingle();

        Container.Bind<NpcContainerContext>().FromInstance(npcContainerContext).AsSingle();

        Container.Bind<IStatsProvider>().To<NpcStatsProvider>().AsSingle();

        Container.Bind<NavMeshAgent>().FromComponentOnRoot().AsSingle();

        Container.BindInterfacesAndSelfTo<NpcBrain>().AsSingle();
        
        Container.Bind<IUnitInteractionSystem>().To<UnitInteractionSystem>().AsSingle();


        Container.BindInterfacesAndSelfTo<NpcNavSystem>().FromNewComponentOnRoot().AsSingle().NonLazy();
    }
}


public class NpcDisplayDataProvider : IUnitDisplayDataProvider
{
    public NpcDisplayDataProvider(string displayName)
    {
        DisplayName = displayName;
    }

    public string DisplayName { get; }
}