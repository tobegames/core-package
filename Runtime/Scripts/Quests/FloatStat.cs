﻿using System;
using UnityEngine;

[Serializable]
public class FloatStat : UnitStat
{
    [SerializeField] private float minValue;
    [SerializeField] private float maxValue;
    [SerializeField] private float defaultValue;

    public float CurrentValue { get; private set; }

    public float DefaultValue => defaultValue;

    public float MinValue => minValue;

    public float MaxValue => maxValue;

    public FloatStat(UnitStatMetadata unitStatMetadata) : base(unitStatMetadata)
    {
    }

    public override void SetValue(object value)
    {
        var val = (float) value;

        CurrentValue = val;

        if (CurrentValue <= minValue)
        {
            CurrentValue = minValue;

            Depleted?.OnNext(this);
        }

        if (CurrentValue >= maxValue)
        {
            CurrentValue = maxValue;
        }
    }

    public override object GetValue()
    {
        return CurrentValue;
    }

    public override bool IsDepleted()
    {
        return CurrentValue <= minValue;
    }

}