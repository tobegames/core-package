﻿using System.Collections.Generic;
using System.Linq;
using ModestTree;
using UnityEngine;

[CreateAssetMenu(fileName = "Resource Repository", menuName = "Repositories/Resources Repository")]
public class ResourceRepository : ScriptableObject, IResourceRepository
{
    [SerializeField] private ResourceItem[] resourceItems;

    public IEnumerable<ResourceItem> ResourceItems => resourceItems;

    public void Validate()
    {
        if (resourceItems != null && resourceItems.Any())
            Assert.That(resourceItems.GroupBy(x => x.id).All(x => x.Count() == 1),
                "resourceItems.GroupBy(x => x.id).All(x => x.Count() == 1)");
    }
}