﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

public enum RewardTiming
{
    Instant,
    QuestGiver
}

[Serializable]
public class QuestAchievementData
{
    public Achievement grantAchievement;

    public RewardTiming rewardTiming;
}

[CreateAssetMenu]
public class QuestData : ScriptableObject
{
    public string id;

    public string title;

    [TextArea] public string description;

    public RewardTiming rewardTiming = RewardTiming.Instant;

    public QuestCondition[] successConditions;

    public QuestAchievementData[] questAchievementDatas;

    public QuestRewardResource[] questRewardResources;

    public DialogLine completionDialog;
}

[Serializable]
public class QuestRewardResource
{
    public ResourceItem resourceItem;

    [ShowIf(nameof(resourceItem))]
    public int amount = 1;
}