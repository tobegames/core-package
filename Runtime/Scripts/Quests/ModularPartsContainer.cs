﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ModularPartsContainer : IModularPart
{
    private const string IndexRegex = @"\d{2}$";

    public int CurrentPartIndex
    {
        get => _currentPartIndex;
        set
        {
            if (_currentPartIndex == value) return;
            HideIndex(_currentPartIndex);
            _currentPartIndex = value;
            ActivateIndex(_currentPartIndex);
        }
    }

    public string PartTag { get; }
    public int Index { get; }
    public IEnumerable<GameObject> Items => _pieces;

    public void ShowNext()
    {
        if (_currentPartIndex + 1 >= _pieces.Length)
        {
            HideIndex(_currentPartIndex);
            _currentPartIndex = -1;
        }

        CurrentPartIndex++;
    }

    public void ShowPrev()
    {
        if (_currentPartIndex - 1 < 0)
        {
            HideIndex(_currentPartIndex);
            _currentPartIndex = _pieces.Length;
        }

        CurrentPartIndex--;
    }

    public void Show()
    {
        ActivateIndex(_currentPartIndex);
    }

    public void Hide()
    {
        HideIndex(_currentPartIndex);
    }

    private GameObject[] _pieces;

    private int _currentPartIndex = -1;

    private GameObject _root;

    public ModularPartsContainer(GameObject gameObject, int defIndex = 0)
    {
        Init(gameObject, defIndex);
    }

    public ModularPartsContainer(string prefix, int index, string partTag, GameObject root, int defIndex = 0)
    {
        Index = index;
        var gameObject = root.transform.Find($"{prefix}_{index:00}_{partTag}").gameObject;

        PartTag = partTag;

        Init(gameObject, defIndex);
    }

    private void Init(GameObject gameObject, int defIndex)
    {
        _root = gameObject;


        _pieces = new GameObject[gameObject.transform.childCount];


        for (int i = 0; i < _pieces.Length; i++)
        {
            _pieces[i] = gameObject.transform.GetChild(i).gameObject;
        }

        //For facial hair there has to be first null to turn it off
        var hasDefaultGameObject = gameObject.transform.GetChild(0).name.EndsWith("00");
        if (!hasDefaultGameObject)
        {
            _pieces = _pieces.Prepend(null).ToArray();
        }

        HideAll();

        CurrentPartIndex = defIndex;
    }

    private void ActivateIndex(int index)
    {
        if (index >= 0 && index <= _pieces.Length)
        {
            var piece = _pieces[index];
            if (piece)
            {
                if (!_root.activeSelf)
                {
                    _root.SetActive(true);
                }
                _pieces[index].SetActive(true);
            }
            else
            {
                if (!_root.activeSelf)
                {
                    _root.SetActive(false);
                }
            }
        }
    }

    private void HideAll()
    {
        foreach (var gameObject in _pieces.Where(x => x))
        {
            gameObject.SetActive(false);
        }
    }

    private void HideIndex(int index)
    {
        if (index >= 0 && index < _pieces.Length)
        {
            if (_pieces[index])
            {
                if (!_root.activeSelf)
                {
                    _root.SetActive(true);
                }

                _pieces[index].SetActive(false);
            }
            else
            {
                _root.SetActive(false);
            }
        }
    }
}