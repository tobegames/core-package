﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

public enum ResourceType
{
    Currency
}


[CreateAssetMenu(menuName = "Resources/Item")]
public class ResourceItem : ScriptableObject
{
    public string id;

    public string displayName;

    public ResourceType resourceType;

    public Sprite icon;

    [BoxGroup("Currency"), ShowIf(nameof(resourceType), ResourceType.Currency)]
    public int maxStack = 1;

    public ResourceItem expensiveCurrency;
}

public interface IPlayerResourcesManager
{
    void GiveResource(string id, int amount);

    IEnumerable<(PlayerResourceItem playerResourceItem, ResourceItem resourceItem)> Currency { get; }
}


public class PlayerResourcesManager : IPlayerResourcesManager
{
    private readonly IResourceRepository _resourceRepository;

    private readonly IPlayerResourcesRepository _playerResourcesRepository;

    public IEnumerable<(PlayerResourceItem playerResourceItem, ResourceItem resourceItem)> Currency =>
        _playerResourcesRepository.Resources.Join(
            _resourceRepository.ResourceItems, item => item.Id, item => item.id,
            (playerResItem, resourceItem) => (playerResItem, resourceItem));

    public PlayerResourcesManager(IResourceRepository resourceRepository,
        IPlayerResourcesRepository playerResourcesRepository)
    {
        _resourceRepository = resourceRepository;
        _playerResourcesRepository = playerResourcesRepository;
    }

    public void GiveResource(string id, int amount)
    {
        var resource = _resourceRepository.ResourceItems.FirstOrDefault(x => x.id.Equals(id));

        if (resource == null)
        {
            throw new KeyNotFoundException("Resource with id " + id + " was not found in repository!");
        }

        var playerResource = _playerResourcesRepository.Resources.FirstOrDefault(x => x.Id.Equals(id));

        if (playerResource == null)
        {
            _playerResourcesRepository.Resources = _playerResourcesRepository.Resources.Append(playerResource =
                new PlayerResourceItem
                {
                    Id = id,
                    Amount = 0,
                }).ToArray();
        }

        // Todo: create currency resource service
        if (resource.resourceType == ResourceType.Currency)
        {
            var newAmount = playerResource.Amount + amount;

            if (newAmount > resource.maxStack)
            {
                if (resource.expensiveCurrency == null)
                {
                    playerResource.Amount = resource.maxStack;
                }
                else
                {
                    var cycle = Mathf.RoundToInt((float) newAmount / resource.maxStack);

                    GiveResource(resource.expensiveCurrency.id, cycle);

                    var realAmount = newAmount - cycle * resource.maxStack;

                    playerResource.Amount = realAmount;
                }
            }
            else
            {
                playerResource.Amount = newAmount;
            }
        }

        _playerResourcesRepository.SaveResources();
    }
}

public class PlayerResourceItem
{
    public string Id { get; set; }

    public int Amount { get; set; }
}

public interface IPlayerResourcesRepository
{
    PlayerResourceItem[] Resources { get; set; }
    void SaveResources();
}

public interface IResourceRepository : IValidatable
{
    IEnumerable<ResourceItem> ResourceItems { get; }
}