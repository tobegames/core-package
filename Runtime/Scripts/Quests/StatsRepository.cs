﻿using UnityEngine;

[CreateAssetMenu(menuName = "Repositories/Stats Repository")]
public class StatsRepository : ScriptableObject
{
    public UnitStatMetadata[] allKnownStats;
}