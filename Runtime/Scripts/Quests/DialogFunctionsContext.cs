﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public  class DialogFunctionsContext : MonoBehaviour, IDialogFunctionsContext
{
    
    public IEnumerable<string> GetAvailibleMethods()
    {
        var currentType = GetType();

        print(currentType.Name);

        return currentType.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly).Select(
            x => x.Name);
    }
    
    
    
    //
    // public GameObject GameObject => gameObject;
    //
    // public abstract void CallMethod(string methodName, object param);
}