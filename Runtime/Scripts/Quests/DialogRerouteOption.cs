﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public enum MethodParam
{
    None = 0,
    UnityObject = 1,
    StringValue = 2,
    NumberValue = 3,
}

[Serializable]
public class DialogRerouteOption
{
    [Header("Option")] public string text;

    [Header("Functionality")] public DialogOptionType optionType;

    [ShowIf(nameof(optionType), DialogOptionType.LeadToAnotherDialog)]
    public DialogLine dialog;

    [ShowIf(nameof(optionType), DialogOptionType.CustomEventHandler)]
    public DialogRerouteOptionEventHandler onOptionSelected;


    [ShowIf(nameof(optionType), DialogOptionType.CustomFunctionCall)]
    public DialogFunctionsContext dialogContext;

    [ShowIf(nameof(optionType), DialogOptionType.CustomFunctionCall)]
    [ShowIf(nameof(dialogContext))]
    [ValueDropdown(nameof(GetDialogMethods))]
    public string methodName;

    [ShowIf(nameof(dialogContext))] [ShowIf(nameof(optionType), DialogOptionType.CustomFunctionCall)]
    public MethodParam methodParam;

    public IEnumerable<string> GetDialogMethods()
    {
        if (!dialogContext) return Enumerable.Empty<string>();

        return dialogContext.GetAvailibleMethods();
    }

    [ShowIf(nameof(optionType), DialogOptionType.CustomFunctionCall)]
    [ShowIf(nameof(dialogContext))]
    [ShowIf(nameof(methodParam), MethodParam.UnityObject)]
    public UnityEngine.Object objectValue;

    [ShowIf(nameof(optionType), DialogOptionType.CustomFunctionCall)]
    [ShowIf(nameof(dialogContext))]
    [ShowIf(nameof(methodParam), MethodParam.StringValue)]
    public string stringValue;

    [ShowIf(nameof(optionType), DialogOptionType.CustomFunctionCall)]
    [ShowIf(nameof(dialogContext))]
    [ShowIf(nameof(methodParam), MethodParam.NumberValue)]
    public float numberValue;
}