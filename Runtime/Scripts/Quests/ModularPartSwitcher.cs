﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class ModularPartSwitcher : MonoBehaviour
{
    [ValueDropdown(nameof(Parts))] [SerializeField]
    private string partName;

    private IEnumerable<string> Parts()
    {
        return ModularPartHelpers.Tags(Gender.AllGender).Union(ModularPartHelpers.Tags(Gender.Male));
    }
    
    public Button next;
    
    public Button prev;

    public ModularCharacterRig rig;

    private void Start()
    {
        next.onClick.AsObservable().Subscribe(x => rig.NextPart(partName));
        prev.onClick.AsObservable().Subscribe(x => rig.PrevPart(partName));
    }
}