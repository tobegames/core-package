﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tobe.Core;
using UnityEngine;
using UniRx;
using Zenject;

public interface IPlayerQuestsRepository
{
    PlayerQuestData[] AllPlayerQuests { get; set; }
    void SaveQuestData();
}

public class ItemStashedGlobalMessage
{
    public IStashableItem StashableItem { get; set; }
}

public class QuestManager : IQuestManager
{
    private readonly IPlayerQuestsRepository _playerQuestsRepository;
    private readonly IQuestRepository _questsRepository;
    private readonly IAchievementService _achievementService;
    private readonly IPlayerResourcesManager _playerResourcesManager;

    public IObservable<QuestData> QuestCompleted => _questCompleted;

    private Subject<QuestData> _questCompleted;

    public IEnumerable<PlayerQuestData> IncompleteQuests =>
        _playerQuestsRepository.AllPlayerQuests.Where(x => !x.IsCompleted);

    public IEnumerable<PlayerQuestData> TakenQuests => _playerQuestsRepository.AllPlayerQuests;

    public QuestManager(IPlayerQuestsRepository playerPlayerQuestsesRepository,
        SignalBus signalBus,
        IQuestRepository questRepository,
        IAchievementService achievementService, IPlayerResourcesManager playerResourcesManager)
    {
        _questCompleted = new Subject<QuestData>();
        _playerQuestsRepository = playerPlayerQuestsesRepository;

        _questsRepository = questRepository;
        _achievementService = achievementService;
        _playerResourcesManager = playerResourcesManager;

        MessageBroker.Default.Receive<ItemStashedGlobalMessage>().Subscribe(x => OnItemStashed(x.StashableItem));
        signalBus.Subscribe<QuestKillProgressSignal>(QuestProgressSignal);
    }

    private void QuestProgressSignal(QuestKillProgressSignal obj)
    {
        var incompleteQuests = IncompleteQuests.SelectMany(x => x.QuestConditionPairs);
        if (!string.IsNullOrEmpty(obj.TargetId))
        {
            var playerQuestConditionPairs = incompleteQuests.Where(x => x.targetId.Equals(obj.TargetId));
            if (!playerQuestConditionPairs.Any()) return;
            
            foreach (var incompleteQuest in playerQuestConditionPairs)
            {
                incompleteQuest.collectedAmount += obj.Amount;
                CheckQuestCompletion(incompleteQuest.questId);
            }
        }
    }

    private void OnItemStashed(IStashableItem obj)
    {
        foreach (var incompleteQuest in IncompleteQuests.Join(_questsRepository.AllQuests, data => data.QuestId,
            data => data.id, (data, questData) => new {data, questData}))
        {
            foreach (var questCondition in incompleteQuest.questData.successConditions.Where(x =>
                x.type == QuestConditionType.Item &&
                obj.Id == x.itemId))
            {
                incompleteQuest.data.QuestConditionPairs.First(x => x.triggerId == questCondition.triggerId)
                    .collectedAmount++;
            }

            CheckQuestCompletion(incompleteQuest.questData.id);
        }
    }


    public void TakeQuest(QuestData currentQuest)
    {
        _playerQuestsRepository.AllPlayerQuests = _playerQuestsRepository.AllPlayerQuests.Append(new PlayerQuestData
        {
            QuestId = currentQuest.id,
            IsCompleted = false,
            QuestConditionPairs = currentQuest.successConditions.Select(questCondition => new PlayerQuestConditionPair
            {
                collectedAmount = 0,
                triggerId = questCondition.triggerId,
                questId = currentQuest.id,
                itemId = questCondition.itemId,
                resourceId = questCondition.resourceItem?.id,
                targetId = questCondition.targetId
            }).ToArray()
        }).ToArray();
    }

    public bool CanCollectReward(string currentQuestId)
    {
        var quest = _playerQuestsRepository.AllPlayerQuests.FirstOrDefault(x => x.QuestId.Equals(currentQuestId));

        if (quest == null) return false;

        return quest.IsCompleted && !quest.IsRewardCollected;
    }

    public bool CanAcceptQuest(string currentQuestId)
    {
        return !_playerQuestsRepository.AllPlayerQuests.Any(x => x.QuestId.Equals(currentQuestId));
    }

    public void CollectReward(string currentQuestId)
    {
        var playerQuest = _playerQuestsRepository.AllPlayerQuests.First(x => x.QuestId.Equals(currentQuestId));

        if (playerQuest.IsRewardCollected) return;

        var questData = _questsRepository.AllQuests.FirstOrDefault(x => x.id.Equals(playerQuest.QuestId));
        //Todo: instant reward type, now its only gived when at quest giver

        if (questData && questData.questRewardResources != null && questData.questRewardResources.Length > 0)
        {
            foreach (var questDataRewardResource in questData.questRewardResources)
            {
                _playerResourcesManager.GiveResource(questDataRewardResource.resourceItem.id,
                    questDataRewardResource.amount);
            }
        }

        playerQuest.IsRewardCollected = true;
    }

    public void CheckQuestCompletion(string questId)
    {
        var questData = _questsRepository.AllQuests.FirstOrDefault(x => x.id.Equals(questId));

        if (questData == null)
        {
            throw new Exception("Quest (id: " + questId + ") not found in repository!");
        }

        var playerQuest = _playerQuestsRepository.AllPlayerQuests.FirstOrDefault(x => x.QuestId.Equals(questData.id));

        if (playerQuest == null || playerQuest.IsCompleted)
        {
            Debug.LogWarning("Cannot check quest " + questData.id + " because it's not given to player.");
            return;
        }


        var conditionPairs =
            playerQuest.QuestConditionPairs
                .Join(questData.successConditions,
                    pair => pair.triggerId,
                    condition => condition.triggerId,
                    (playerQuestField, questDataField) =>
                        new {playerQuestField, questDataField});

        playerQuest.IsCompleted =
            conditionPairs.All(x => x.playerQuestField.collectedAmount >= x.questDataField.amount);

        if (playerQuest.IsCompleted)
        {
            if (questData.questAchievementDatas != null && questData.questAchievementDatas.Length > 0)
            {
                foreach (var achievementData in questData.questAchievementDatas.Where(x =>
                    x.rewardTiming == RewardTiming.Instant))
                {
                    _achievementService.GiveAchievement(achievementData.grantAchievement);
                }
            }

            _questCompleted.OnNext(questData);
        }
    }
}