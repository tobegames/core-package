﻿using System;

public static class ModularPartHelpers
{
    public static string[] Tags(Gender gender)
    {
        switch (gender)
        {
            case Gender.Male:
            case Gender.Female:
                return MaleFemaleTags;
            case Gender.AllGender:
                return AllGenderTags;
            default:
                throw new ArgumentOutOfRangeException(nameof(gender), gender, null);
        }
    }

    private static readonly string[] AllGenderTags = new[]
    {
        "HeadCoverings",
        "Hair"
    };    
    private static readonly string[] MaleFemaleTags = new[]
    {
        "Head",
        "Eyebrows",
        "FacialHair",
        "Torso",
        "Arm_Upper_Right",
        "Arm_Upper_Left",
        "Arm_Lower_Right",
        "Arm_Lower_Left",
        "Hand_Right",
        "Hand_Left",
        "Hips",
        "Leg_Right",
        "Leg_Left"
    };
}