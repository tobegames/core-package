﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(menuName = "Modular Characters/Default Layout")]
public class ModularCharacterLayout : ScriptableObject
{
    public Transform modularPrefab;

    [ValueDropdown(nameof(GetDropdownValues))]
    public string head;

    [ValueDropdown(nameof(GetDropdownValues))]
    public string eyebrows;
    [ValueDropdown(nameof(GetDropdownValues))]
    public string facialHair;
    [ValueDropdown(nameof(GetDropdownValues))]
    public string torso;
    [ValueDropdown(nameof(GetDropdownValues))]
    public string upperArmRight;
    [ValueDropdown(nameof(GetDropdownValues))]
    public string upperArmLeft;
    [ValueDropdown(nameof(GetDropdownValues))]
    public string lowerArmRight;
    [ValueDropdown(nameof(GetDropdownValues))]
    public string lowerArmLeft;
    [ValueDropdown(nameof(GetDropdownValues))]
    public string handRight;
    [ValueDropdown(nameof(GetDropdownValues))]
    public string handLeft;
    [ValueDropdown(nameof(GetDropdownValues))]
    public string hips;
    [ValueDropdown(nameof(GetDropdownValues))]
    public string legRight;
    [ValueDropdown(nameof(GetDropdownValues))]
    public string legLeft;

    private string[] _parts;

    [Button]
    public void SetFromPrefab()
    {
        if (!modularPrefab) return;

        foreach (Transform transform in modularPrefab)
        {
            PopulateFields(transform);
        }
    }

    private void PopulateFields(Transform root)
    {
        foreach (Transform transform in root)
        {
            var partName = Inspect(transform);

            if (string.IsNullOrEmpty(partName)) continue;

            var lower = partName.ToLower();

            if (lower.Contains("head"))
            {
                head = partName;
                continue;
            }

            if (lower.Contains("eyebrow"))
            {
                eyebrows = partName;
                continue;
            }

            if (lower.Contains("facialhair"))
            {
                facialHair = partName;
                continue;
            }

            if (lower.Contains("torso"))
            {
                torso = partName;
                continue;
            }

            if (lower.Contains("armupperright"))
            {
                upperArmRight = partName;
                continue;
            }

            if (lower.Contains("armupperleft"))
            {
                upperArmLeft = partName;
                continue;
            }

            if (lower.Contains("armlowerleft"))
            {
                lowerArmLeft = partName;
                continue;
            }

            if (lower.Contains("armlowerright"))
            {
                lowerArmRight = partName;
                continue;
            }

            if (lower.Contains("handright"))
            {
                handRight = partName;
                continue;
            }

            if (lower.Contains("handleft"))
            {
                handLeft = partName;
                continue;
            }

            if (lower.Contains("hips"))
            {
                hips = partName;
                continue;
            }

            if (lower.Contains("legleft"))
            {
                legLeft = partName;
                continue;
            }

            if (lower.Contains("legright"))
            {
                legRight = partName;
                continue;
            }
        }
    }

    private string Inspect(Transform root)
    {
        if (!root.gameObject.activeInHierarchy)
            return null;

        if (root.childCount == 0)
        {
            return root.gameObject.name;
        }

        foreach (Transform transform in root)
        {
            var value = Inspect(transform);
            if (!string.IsNullOrEmpty(value))
                return value;
        }

        return null;
    }

    private IEnumerable<string> GetDropdownValues()
    {
        if (_parts == null)
            InitParts();

        return _parts;
    }

    private void InitParts()
    {
        if (!modularPrefab) return;

        var result = new List<string>();

        GetPathPart(modularPrefab.transform, result);

        _parts = result.ToArray();
    }

    private void GetPathPart(Transform root, ICollection<string> result)
    {
        if (root.childCount > 0)
        {
            foreach (Transform child in root)
            {
                GetPathPart(child, result);
            }
        }

        result.Add(root.name);
    }
}