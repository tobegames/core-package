﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tobe.Core;
using UniRx;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

public interface ICommandListener
{
    void ProcessCommand(ICommand command);
}

public class NpcNavSystem : MonoBehaviour, ICommandListener
{
    private NavMeshAgent _agent;

    public IObservable<PatrolPoint> OnPointReached => _pointReached;

    private Subject<PatrolPoint> _pointReached;

    private CircularList<PatrolPoint> _patrolPoints;

    private IAiTarget _followTarget;

    [Inject]
    private void Ctor(NavMeshAgent agent,
        ICommander commander)
    {
        _agent = agent;

        commander.CurrentCommand
            .Subscribe(ProcessCommand);

        _pointReached = new Subject<PatrolPoint>();
    }

    private PatrolPoint _currentPatrolPoint;

    private void Update()
    {
        if (!_agent.isOnNavMesh || _agent.isStopped)
        {
            print("Agent on navmesh " + _agent.isOnNavMesh);
            print("Agent is stopped " + _agent.isStopped);
            return;
        }


        if (_currentPatrolPoint)
        {
            var dist = Vector3.Distance(transform.position, _currentPatrolPoint.transform.position);

            if (dist <= _currentPatrolPoint.Range)
            {
                _agent.isStopped = true;

                _agent.destination = transform.position;

                _pointReached.OnNext(_currentPatrolPoint);

                StartCoroutine(WaitCoroutine(_currentPatrolPoint));
            }
            else
            {
                _agent.destination = _currentPatrolPoint.transform.position;
            }
        }
        else if (_followTarget != null)
        {
            _agent.destination = _followTarget.WorldPosition;
        }
       
    }

    private IEnumerator WaitCoroutine(PatrolPoint currentCheckpoint)
    {
        yield return new WaitForSeconds(currentCheckpoint.idleSeconds);

        _currentPatrolPoint = _patrolPoints.GetNext(currentCheckpoint);

        _agent.isStopped = false;

        _agent.destination = _currentPatrolPoint.transform.position;
    }

    public void ProcessCommand(ICommand command)
    {
        if (command is PatrolCommand patrolCommand)
        {
            _patrolPoints = new CircularList<PatrolPoint>(patrolCommand.PatrolPoints);
            _currentPatrolPoint = _patrolPoints.CurrentItem;
            _agent.isStopped = false;
            _agent.updateRotation = true;
        }

        if (command is DialogCommand dialogCommand)
        {
            _agent.updateRotation = false;
            _agent.transform.rotation = Quaternion.LookRotation(new Vector3(dialogCommand.InitiatorPosition.x,
                _agent.transform.position.y, dialogCommand.InitiatorPosition.z));
            _agent.destination = transform.position;
            _agent.isStopped = true;
        }

        if (command is KillCommand killCommand)
        {
            print("Kill command");
            _agent.isStopped = false;
            _followTarget = killCommand.Target;
        }
    }
}