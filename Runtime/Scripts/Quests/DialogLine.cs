﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable]
public class DialogLine
{
    public DialogType type;
    
    [Tooltip("Just for editor")]
    public string inspectorLabel = "Dialog line";

    [ShowIf(nameof(type), DialogType.Text)]
    [TextArea]
    public string text;
    
    [ShowIf(nameof(type), DialogType.QuestChoice)]
    public QuestData[] quests;

    [LabelText("Say if no quests availible"),
     ShowIf(nameof(type), DialogType.QuestChoice)]
    public string allQuestsTaken;

    [LabelText("Dialog options")]
    [ShowIf(nameof(type), DialogType.CustomOptions)]
    public DialogRerouteOption[] dialogRerouteOptions;
}