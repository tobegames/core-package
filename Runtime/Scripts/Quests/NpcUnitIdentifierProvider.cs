﻿using System.Linq;

public class NpcUnitIdentifier : IUnitIdentifier
{
    public NpcUnitIdentifier(string unitId, int combatGroupId)
    {
        UnitId = unitId;
        CombatGroupId = combatGroupId;
    }

    public string UnitId { get; }
    public int CombatGroupId { get; }
}

public class EnemyDisplayDataProvider : IUnitDisplayDataProvider
{
    public EnemyDisplayDataProvider(EnemyContainerContext containerContext)
    {
        DisplayName = containerContext.displayName;
    }
    public string DisplayName { get; }
}

public class EnemyStatsProvider : IStatsProvider
{
    public EnemyStatsProvider(EnemyContainerContext containerContext)
    {
        Stats = containerContext.stats;
        LifeStat = containerContext.lifeStat;
    }

    public FloatStat[] Stats { get; }
    public FloatStat LifeStat { get; }
}

public class EnemyUnitIndentifier : IUnitIdentifier
{
    public EnemyUnitIndentifier(string unitId, int combatGroupId)
    {
        UnitId = unitId;
        CombatGroupId = combatGroupId;
    }

    public string UnitId { get; }
    public int CombatGroupId { get; }
}