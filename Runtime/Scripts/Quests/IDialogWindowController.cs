﻿using System;
using Tobe.Core;
using Tobe.Core.Abstractions;
using Tobe.Core.Mvc;
using Tobe.Core.Mvc.Implementations;
using UnityEngine;

[Serializable]
public class DialogWindowView : IView<DialogWindowViewModel>
{
    public TobeLabel textLabel;

    public TobeLabel npcName;


    public TobeButton nextButton;

    public TobeButton cancelButton;

    public DialogListView inDialogListView;
    public GameObject root;

    public void RenderModel(DialogWindowViewModel model)
    {
    }
}

[Serializable]
public class DialogContext
{
    public IUnitIdentifier TargetIdentifier { get; set; }
    public IUnitDisplayDataProvider DisplayDataProvider { get; set; }
    public DialogLine[] DialogLines { get; set; }
}

[Serializable]
public class DialogWindowViewModel : IDirtyModel
{
    public string nextButtonNextText = "Next";
    public string nextButtonCompleteText = "Complete";

    public bool IsDirty { get; set; }
}

public interface IDialogWindowController : IPopupDialog, IController<DialogWindowView, DialogWindowViewModel>
{
    void RenderDialog(DialogContext data);

    void CancelDialog();

    IObservable<DialogContext> OnClosing { get; }
}