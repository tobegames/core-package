﻿using System.Collections.Generic;

public interface IQuestManager
{
    void CheckQuestCompletion(string questId);

    IEnumerable<PlayerQuestData> IncompleteQuests { get; }
    IEnumerable<PlayerQuestData> TakenQuests { get; }
    void TakeQuest(QuestData currentQuest);
    bool CanCollectReward(string currentQuestId);
    bool CanAcceptQuest(string currentQuestId);
    void CollectReward(string currentQuestId);
}