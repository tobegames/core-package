﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Zenject;

public class ModularCharacterRig : MonoBehaviour
{
    public GameObject root;

    public GenderRoot AllGenderRoot { get; private set; }

    public GenderRoot MaleRoot { get; private set; }

    private ModularCharacterRigRepository _repository;

    [Inject]
    private void Ctor(ModularCharacterRigRepository repository)
    {
        _repository = repository;
    }

    private void Start()
    {
        BuildSetup();
        LoadLayout(null);
    }

    private Gender currentGender;

    public void NextPart(string bodyPart)
    {
        MaleRoot.Containers.Union(AllGenderRoot.Containers).FirstOrDefault(x => x.PartTag.Equals(bodyPart))?.ShowNext();
    }

    private void BuildSetup()
    {
        MaleRoot = new GenderRoot(Gender.Male,
            root.transform.Find(Gender.Male.ToModularString() + "_Parts").gameObject);

        AllGenderRoot = new GenderRoot(Gender.AllGender,
            root.transform.Find(Gender.AllGender.ToModularString() + "_Gender_Parts").gameObject);
    }

    private Dictionary<string, int> _currentSetup;

    public void PrevPart(string part)
    {
        MaleRoot.Containers.Union(AllGenderRoot.Containers).FirstOrDefault(x => x.PartTag.Equals(part))?.ShowPrev();
    }

    public void SaveLayout(string guid)
    {
        _repository.SaveLayout(MaleRoot, AllGenderRoot, guid);
    }

    public void LoadLayout(string guid)
    {
        var currentLayout = _repository.LoadLayout(guid);

        if (currentLayout != null)
        {
            foreach (var layoutPartId in currentLayout.PartIds)
            {
                MaleRoot.SetPart(layoutPartId.Key, layoutPartId.Value);
                AllGenderRoot.SetPart(layoutPartId.Key, layoutPartId.Value);
            }
        }
    }

    private readonly Regex _partRegex = new Regex(@"_(?<part>\w+)_(?<gender>\w+)_(?<index>\d{2}$)");

    public void ShowPart(string partFullPath)
    {
        var matches = _partRegex.Matches(partFullPath);

        foreach (Match match in matches)
        {
            var matchGroup = match.Groups["part"];

            var index = int.Parse(match.Groups["index"].Value);
            if (matchGroup.Success)
            {
                var container =
                    MaleRoot.Containers.FirstOrDefault(x => x.PartTag.Replace("_", "").Equals(matchGroup.Value));

                container.CurrentPartIndex = index;
            }
        }
    }
}