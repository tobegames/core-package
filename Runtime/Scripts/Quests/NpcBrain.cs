﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public interface ICommander
{
    IReadOnlyReactiveProperty<ICommand> CurrentCommand { get; }
    bool TryInterupt(ICommand command);
}

public interface ICommand
{
}

public class PatrolCommand : ICommand
{
    public IEnumerable<PatrolPoint> PatrolPoints { get; set; }
}

public class KillCommand :ICommand
{
    public KillCommand(IAiTarget target)
    {
        Target = target;
    }

    public    IAiTarget Target { get; }
}

public class DialogCommand : ICommand
{
    public Vector3 InitiatorPosition { get; set; }
    public IUnitIdentifier UnitIdentifier { get; set; }
}

public class NpcBrain : IAiBrain
{

    public IReadOnlyReactiveProperty<ICommand> CurrentCommand => _currentCommand;

    private readonly ReactiveProperty<ICommand> _currentCommand;

    private ICommand _postponedCommand;

    public NpcBrain(IDialogManager dialogManager, IUnitIdentifier unitIdentifier, IEnumerable<PatrolPoint> patrolPoints = null)
    {
        _currentCommand = new ReactiveProperty<ICommand>();

        dialogManager.DialogComplete.Where(x => x.TargetIdentifier == unitIdentifier).Subscribe(x =>
        {
            if (_postponedCommand != null)
            {
                _currentCommand.Value = _postponedCommand;
                _postponedCommand = null;
            }
        });
        
        if (patrolPoints != null)
        {
            _currentCommand.Value = new PatrolCommand
            {
                PatrolPoints = patrolPoints
            };
        }
    }
    
    
    
    public bool TryInterupt(ICommand command)
    {
        if (_postponedCommand != null)
        {
            Debug.LogWarning("Giving up command " + _postponedCommand.GetType());
        }
        
        _postponedCommand = CurrentCommand.Value;

        _currentCommand.Value = command;

        return true;
    }


    public void Dispose()
    {
        _currentCommand?.Dispose();
    }
}