﻿using System;
using Zenject;
using UniRx;

public interface IDialogManager : IDisposable
{
    IObservable<DialogContext> DialogComplete { get; }
}

public class DialogManager : IDialogManager, IInitializable
{
    private readonly SignalBus _signalBus;

    private readonly IInteractionUiManager _interactionUiManager;

    private readonly Subject<DialogContext> _dialogComplete;
    
    public DialogManager(IInteractionUiManager interactionUiManager, SignalBus signalBus)
    {
        _interactionUiManager = interactionUiManager;

        _signalBus = signalBus;

        _signalBus.Subscribe<DialogOpportunitySignal>(DialogOpportunity);

         _dialogComplete  = new Subject<DialogContext>();
    }

    private void DialogOpportunity(DialogOpportunitySignal obj)
    {
        _interactionUiManager.DialogWindowController.RenderDialog(new DialogContext
        {
            DialogLines = obj.DialogLines,
            TargetIdentifier = obj.Target.UnitIdentifier,
            DisplayDataProvider = obj.Target.DisplayDataProvider,
        });


        _interactionUiManager.DialogWindowController.Open();
    }

    public void Dispose()
    {
    }

    public IObservable<DialogContext> DialogComplete => _dialogComplete;
    public void Initialize()
    {
        _interactionUiManager.DialogWindowController.OnClosing.Subscribe(x => _dialogComplete.OnNext(x));
    }
}