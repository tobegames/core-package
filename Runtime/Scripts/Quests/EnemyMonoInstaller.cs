﻿using UnityEngine.AI;
using Zenject;

public class EnemyMonoInstaller : MonoInstaller<EnemyMonoInstaller>
{
    public EnemyContainerContext npcContainerContext;
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<EnemyContainerContext>().FromInstance(npcContainerContext).AsSingle();
        
        Container.Bind<IUnitIdentifier>().To<EnemyUnitIndentifier>()
            .FromInstance(new EnemyUnitIndentifier(npcContainerContext.unitId, npcContainerContext.combatGroupId))
            .AsSingle();

        Container.BindInterfacesAndSelfTo<NpcNavSystem>().FromNewComponentOnRoot().AsSingle().NonLazy();
        Container.Bind<NavMeshAgent>().FromComponentOnRoot().AsSingle();

        Container.Bind<IStatsProvider>().To<EnemyStatsProvider>().AsSingle();
        Container.Bind<IUnitDisplayDataProvider>().To<EnemyDisplayDataProvider>().AsSingle();
        
        Container.BindInterfacesAndSelfTo<CombatBrain>().AsSingle().NonLazy();
    }
}