﻿using System;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;


[Serializable]
public abstract class UnitStat : IDisposable
{
    protected readonly Subject<UnitStat> Depleted;

    [SerializeField, ReadOnly]
    private  UnitStatMetadata unitStatMetadata;
   
    public string Id => unitStatMetadata == null ? null : unitStatMetadata.id;

    [PreviewField]
    public Sprite Icon => unitStatMetadata == null ? null : unitStatMetadata.icon;
    
    
    protected UnitStat(UnitStatMetadata unitStatMetadata)
    {
        this.unitStatMetadata = unitStatMetadata;
        
        Depleted = new Subject<UnitStat>();
    }

    public IObservable<UnitStat> OnDepleted => Depleted;

    public abstract void SetValue(object value);

    public abstract object GetValue();

    public void Dispose()
    {
        Depleted?.Dispose();
    }

    public abstract bool IsDepleted();
}