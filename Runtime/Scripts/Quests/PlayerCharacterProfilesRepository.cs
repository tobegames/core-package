﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;

public class PlayerCharacterProfilesRepository : IDisposable
{
    private const string mapKey = "player.character.profiles";
    private const string currentProfileKey = "player.character.profiles.active";

    private Dictionary<string, string> _map;

    public IReadOnlyDictionary<string, string> Map => _map ?? (_map = LoadMap());

    public string ActiveProfileKey
    {
        get => PlayerPrefs.GetString(currentProfileKey);
        set => PlayerPrefs.SetString(currentProfileKey, value);
    }

    public bool HasValue(string value)
    {
        return Map.Values.Any(x => x.Equals(value));
    }

    public string AddValue(string value)
    {
        if (HasValue(value)) throw new DuplicateNameException(value);

        var guid = Guid.NewGuid().ToString("N");

        if (_map == null)
            _map = LoadMap();

        _map.Add(guid, value);

        SaveKeys(_map);

        return guid;
    }

    public void UpdateValue(string key, string value)
    {
        if (!HasValue(value) || !Map.ContainsKey(key)) return;

        _map[key] = value;

        SaveKeys(_map);
    }

    public void DeleteValue(string key)
    {
        if (!Map.ContainsKey(key)) return;
        ActiveProfileKey = null;
        _map.Remove(key);
        SaveKeys(_map);
    }

    private Dictionary<string, string> LoadMap()
    {
        var jsonVal = PlayerPrefs.GetString(mapKey);

        if (string.IsNullOrEmpty(jsonVal)) return new Dictionary<string, string>();

        return JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonVal);
    }

    private void SaveKeys(Dictionary<string, string> map)
    {
        var jsonVal = JsonConvert.SerializeObject(map);

        PlayerPrefs.SetString(mapKey, jsonVal);
    }

    public string GetKey(string value)
    {
        return Map.First(x => x.Value.Equals(value)).Value;
    }

    public void Dispose()
    {
        SaveKeys(LoadMap());
    }
}