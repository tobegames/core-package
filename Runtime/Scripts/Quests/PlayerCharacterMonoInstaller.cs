﻿using Zenject;

public class PlayerCharacterMonoInstaller : MonoInstaller<PlayerCharacterMonoInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<ModularCharacterRigRepository>().AsSingle();
        Container.BindInterfacesAndSelfTo<PlayerCharacterProfilesRepository>().AsSingle();
    }
}