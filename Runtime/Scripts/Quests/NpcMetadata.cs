﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AI;

//Todo: npc repository
[CreateAssetMenu(menuName = "Npc/Metadata")]
public class NpcMetadata : ScriptableObject
{
    public string displayName;

    public string npcId;
    
    [ListDrawerSettings(ListElementLabelName = nameof(DialogLine.inspectorLabel), ShowIndexLabels = true)]
    public DialogLine[] dialogLines;
    [Tooltip("This will be used to decide if unit is alive or dead")] [OnValueChanged(nameof(LifeStatChanged))]
    public UnitStatMetadata lifeStatMetadata;

    public FloatStat lifeStat;

    [InfoBox("Drag Stats repository here to edit character stats", InfoMessageType.Warning, nameof(ShowWarning))]
    [SerializeField] private StatsRepository statsRepository;

    [ValueDropdown(nameof(GetStats))] [LabelText("Select stat to add")] [SerializeField] [ShowIf(nameof(statsRepository))]
    private UnitStatMetadata selectedStat;

    [ListDrawerSettings(CustomAddFunction = nameof(AddStat)), EnableIf(nameof(EnableAdd))]
    public FloatStat[] stats;
    private bool EnableAdd => statsRepository && selectedStat;
    private bool ShowWarning => statsRepository == null;

    private FloatStat AddStat()
    {
        if (selectedStat != null && !string.IsNullOrEmpty(selectedStat.id))
        {
            return new FloatStat(selectedStat);
        }

        return null;
    }

    private IEnumerable<UnitStatMetadata> GetStats()
    {
        return statsRepository.allKnownStats;
    }

    private void LifeStatChanged()
    {
        lifeStat = !lifeStatMetadata ? null : new FloatStat(lifeStatMetadata);

        if (lifeStat != null)
            MonoBehaviour.print(lifeStat.Id);
    }
}



public interface IUnitIdentifier
{
    string UnitId { get; }
    
    int CombatGroupId { get; }
}

public interface IUnitDisplayDataProvider
{
    string DisplayName { get; }
}

[Serializable]
public class NpcContainerContext
{
    public NpcMetadata npcMetadata;
}