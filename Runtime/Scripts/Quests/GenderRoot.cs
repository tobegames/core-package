﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GenderRoot
{
    public Gender Gender { get; }

    private GameObject Root { get; }

    private readonly IModularPart[] _containers;

    public IEnumerable<IModularPart> Containers => _containers;

    public void SetPart(string tag, int index)
    {
        var container = Containers.FirstOrDefault(x => x.PartTag.Equals(tag));
        if (container == null) return;
        container.CurrentPartIndex = index;
    }

    public GenderRoot(Gender gender, GameObject gameObject)
    {
        var tags = ModularPartHelpers.Tags(gender);
        _containers = new IModularPart[tags.Length];

        Root = gameObject;

        Gender = gender;

        var prefix = gender.ToModularString();

        for (int i = 0; i < _containers.Length; i++)
        {
            var partTag = tags[i];

            if (partTag == "Head")
            {
                _containers[i] = new ModularHeadPartsContainer(gender, Root, partTag);
            }
            else
            {
                _containers[i] = new ModularPartsContainer(prefix, i, partTag, Root);
            }
        }
    }
}