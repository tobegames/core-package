﻿using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine.EventSystems;

public class ObservablePointerUpTrigger : ObservableTriggerBase, IPointerDownHandler, IPointerUpHandler
{
    private Subject<PointerEventData> _onLongPointerDown;

    private PointerEventData _lastPointerEventData;

    private bool _isDown;

    private void Update()
    {
        if (!_isDown && _lastPointerEventData != null)
            _onLongPointerDown?.OnNext(_lastPointerEventData);
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        _lastPointerEventData = eventData;
        _isDown = true;
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        _lastPointerEventData = eventData;
        _isDown = false;
    }

    public IObservable<PointerEventData> OnPointerUpAsObservable()
    {
        return _onLongPointerDown ?? (_onLongPointerDown = new Subject<PointerEventData>());
    }

    protected override void RaiseOnCompletedOnDestroy()
    {
        _onLongPointerDown?.OnCompleted();
    }
}