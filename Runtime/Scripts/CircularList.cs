﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Tobe.Core
{
    public class CircularList<TModel> : IList<TModel>, IDisposable
    {
        private readonly IList<TModel> _list;

        private int Index { get; set; }

        public CircularList(IEnumerable<TModel> items = null)
        {
            _list = items == null ? new List<TModel>() : new List<TModel>(items);
        }

        public TModel CurrentItem
        {
            get
            {
                if (_list.Count == 0) return default;
                
                if (Index >= _list.Count)
                {
                    Index = 0;
                }
                else if (Index < 0)
                {
                    Index = _list.Count - 1;
                }

                return this[Index];
            }
        }

        public TModel MoveNext()
        {
            Index++;
            return CurrentItem;
        }

        public TModel MovePrev()
        {
            Index--;
            return CurrentItem;
        }

        public IEnumerator<TModel> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(TModel item)
        {
            _list.Add(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(TModel item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(TModel[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public bool Remove(TModel item)
        {
            return _list.Remove(item);
        }

        public int Count => _list.Count;

        public bool IsReadOnly => _list.IsReadOnly;

        public int IndexOf(TModel item)
        {
            return _list.IndexOf(item);
        }

        public void Insert(int index, TModel item)
        {
            _list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _list.RemoveAt(index);
        }

        public TModel this[int index]
        {
            get => _list[index];
            set => _list[index] = value;
        }

        public void Dispose()
        {
            _list.Clear();
        }

        public TModel GetNext(TModel item)
        {
            var index = _list.IndexOf(item);
            if (index >= Count - 1)
            {
                return _list[0];
            }

            return _list[index + 1];
        }
    }
}