﻿using System;
using UnityEngine;

public class TrackTarget : MonoBehaviour
{
    [SerializeField]
    private Transform target;

    private Quaternion _initRotation;

    private void Awake()
    {
        _initRotation = transform.localRotation;
    }

    private void Update()
    {
        if (target)
        {
            var targetPosition = target.position;
            
            var lookPoint = new Vector3(targetPosition.x, transform.position.y, targetPosition.z);
            
            transform.LookAt(lookPoint);
        }
        else
        {
            transform.localRotation = _initRotation;
        }
    }

    public Transform GetTarget()
    {
        return target;
    }
}