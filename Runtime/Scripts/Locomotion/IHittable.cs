﻿public interface IHittable
{
    void TakeHit(IDamageSource damageSource, int damageAmount);
    IUnitIdentifier UnitIdentifier { get; }
}

public interface IDamageSource
{
    IUnitIdentifier UnitIdentifier { get; }
}