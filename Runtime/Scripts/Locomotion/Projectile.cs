﻿using System;
using System.Linq;
using Sirenix.Utilities;
using Tobe.Core;
using UniRx;
using UnityEngine;
using Zenject;


[RequireComponent(typeof(ReactiveProximityWatcher), typeof(Rigidbody))]
public class Projectile : MonoBehaviour, IPoolable<ProjectileSpawnArgs, IMemoryPool>, IDisposable
{
    public float speed = 1;

    [SerializeField] private bool useRayCast;

    [SerializeField] private LayerMask rayCastLayers;
    private ReactiveProximityWatcher _proximityWatcher;
    public float lifeTimeSeconds = 1;
    private readonly RaycastHit[] _hits = new RaycastHit[6];

    private bool isActive = true;

    private IDamageSource _damageSource;

    [SerializeField] private bool despawnOnHit = true;

    [Inject]
    private void Ctor(ProjectileRepository projectileRepository)
    {
        _proximityWatcher = GetComponent<ReactiveProximityWatcher>();

        foreach (var prefab in projectileRepository.items)
        {
            Instantiate(prefab, transform, false);
        }

       
    }

    private void Awake()
    {
        if (useRayCast)
        {
            var ray = new Ray(transform.position, transform.forward);
         
            Debug.DrawRay(ray.origin, ray.direction, Color.red, 5);
         
            var hitted = Physics.RaycastNonAlloc(ray, _hits, 100, rayCastLayers);
         
            if (hitted > 0)
            {
                for (int i = 0; i < hitted; i++)
                {
                    var hit = _hits[i];
         
                    var delegatedCollider = hit.collider.GetComponent<DelegatedCollider>();
         
                    if (delegatedCollider)
                    {
                        print(delegatedCollider + " " + delegatedCollider.Parent);
                    }
         
                    var target = delegatedCollider ? delegatedCollider.Parent : hit.collider.gameObject;
         
                    GiveHit(1, target);
                }
            }
        }
        else
        {
            _proximityWatcher.OnGameObjectEnter.Where(x=> _damageSource != null).Subscribe(data =>
            {
                var hittable = data.GameObject.GetComponent<IHittable>();
                
                if (hittable == null || hittable.UnitIdentifier == _damageSource.UnitIdentifier) return;
                
                print("Projectile triggered with " + data.GameObject);
         
                GiveHit(1, data.GameObject);
            });
        }
    }

    public void GiveHit(int damageAmount, GameObject collision)
    {
        var hittable = collision.GetComponentsInterfaced<IHittable>().FirstOrDefault();

        if (hittable != null)
        {
            print("Hitted entity " + collision);

            hittable.TakeHit(_damageSource, damageAmount);
        }

        if (despawnOnHit)
        {
            Dispose();
        }
    }

    private void Update()
    {
        if (useRayCast || !isActive) return;
        transform.position += speed * Time.deltaTime * transform.forward;
    }

    public void Dispose()
    {
        _pool?.Despawn(this);
    }

    public void OnDespawned()
    {
        _lifetimeCoroutine?.Dispose();

        transform.OfType<Transform>().ForEach(x => x.gameObject.SetActive(false));

        isActive = false;
        _damageSource = null;
        _pool = null;
    }

    private IMemoryPool _pool;

    private IDisposable _lifetimeCoroutine;

    public void OnSpawned(ProjectileSpawnArgs p1, IMemoryPool p2)
    {
        _damageSource = p1.DamageSource;
        transform.position = p1.Position;
        transform.rotation = p1.Rotation;
        _pool = p2;
        isActive = true;

        transform.OfType<Transform>().ForEach(x => x.gameObject.SetActive(false));

        var child = transform.FindChild(x => x.StartsWith(p1.ModelPrefab.name));

        if (!child)
            child = Instantiate(p1.ModelPrefab, transform, false).transform;

        child.gameObject.SetActive(true);

        _lifetimeCoroutine = Observable.Timer(TimeSpan.FromSeconds(p1.Lifetime)).Subscribe(x =>
        {
            if (isActive)
            {
                print("Projectile expried");
                Dispose();
            }
        });
    }

    public class Factory : PlaceholderFactory<ProjectileSpawnArgs, Projectile>
    {
    }
}