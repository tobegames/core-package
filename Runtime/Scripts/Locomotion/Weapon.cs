﻿using UnityEngine;

public abstract class Weapon : MountableItem, IWeapon
{
   
    public AnimatorOverrideController AnimatorOverrideController => animatorOverrideController;


    [SerializeField] private float range = 1f;

    public abstract bool IsMelee { get; }

    public abstract bool CanMelee { get; }
    public abstract bool CanHaveShield { get; }

    public float Range => range;

    public abstract void Use(ItemUsagePhase phase, IUnitIdentifier @groupUsingIt = null);
  
}