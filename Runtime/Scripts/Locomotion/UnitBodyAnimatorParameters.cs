﻿using System;
using UnityEngine;
using Zenject;

[Serializable]
public class UnitBodyAnimatorParameters : IInitializable
{
    [SerializeField] private string velocityXParamName = "Velocity X";
    [SerializeField] private string velocityZParamName = "Velocity Z";
    [SerializeField] private string locomotionVariantParamName = "Locomotion Variant";
    [SerializeField] private string drawWeaponTriggerName = "Draw Weapon";
    [SerializeField] private string disarmWeaponTriggerName = "Disarm Weapon";
    [SerializeField] private string hasWeaponEquipedParamName = "Has Weapon Equipped";
    [SerializeField] private string weaponAttackPhaseTriggerParamName = "Weapon Attack Phase 2";
    [SerializeField] private string cancelWeaponPhaseTriggerParamName = "Cancel Weapon Phase";

    public int VelocityXHandle { get; private set; }

    public int VelocityZHandle { get; private set; }

    public int LocomotionVariant { get; private set; }

    public int DrawWeaponTrigger { get; private set; }

    public int DisarmWeaponTrigger { get; private set; }

    public int HasWeaponEquipped { get; private set; }

    public int WeaponAttackPhaseTrigger { get; private set; }

    public int CancelWeaponPhaseTrigger { get; private set; }
    
    public void Initialize()
    {
        VelocityXHandle = Animator.StringToHash(velocityXParamName);
        VelocityZHandle = Animator.StringToHash(velocityZParamName);
        LocomotionVariant = Animator.StringToHash(locomotionVariantParamName);
        DrawWeaponTrigger = Animator.StringToHash(drawWeaponTriggerName);
        DisarmWeaponTrigger = Animator.StringToHash(disarmWeaponTriggerName);
        HasWeaponEquipped = Animator.StringToHash(hasWeaponEquipedParamName);
        WeaponAttackPhaseTrigger = Animator.StringToHash(weaponAttackPhaseTriggerParamName);
        CancelWeaponPhaseTrigger = Animator.StringToHash(cancelWeaponPhaseTriggerParamName);
    }

}