﻿using Sirenix.OdinInspector;
using UnityEngine;

public class GroupSpawner : MonoBehaviour
{
    public Spawner[] group;

    [Button]
    public void Spawn()
    {
        foreach (var minionsSpawner in @group)
        {
            var minion = minionsSpawner.Spawn();
        }
    }
}