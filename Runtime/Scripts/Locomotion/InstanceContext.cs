﻿using UniRx;

public class InstanceContext : IInstanceContext
{
    public IReadOnlyReactiveCollection<IAiTarget> Targets => _targets;

    private readonly ReactiveCollection<IAiTarget> _targets;

    public InstanceContext()
    {
        _targets = new ReactiveCollection<IAiTarget>();
    }

    public void RegisterTarget(IAiTarget item)
    {
        if (_targets.Contains(item)) return;
        _targets.Add(item);
    }

    public void UnregisterTarget(IAiTarget item)
    {
        if (!_targets.Contains(item)) return;
        _targets.Remove(item);
    }

    public void Dispose()
    {
        _targets?.Dispose();
    }
}