﻿using System;
using UniRx;
using UnityEngine;

public class ItemSystem : IItemSystem
{
    private readonly Transform _root;

    private readonly ItemSlotRig _itemSlotRig;


    public ItemSystem(
        Transform root,
        ItemSlotRig itemSlotRig,
        UnitBodyAnimator unitBodyAnimator,
        IUnitIdentifier combatGroup)
    {
        _itemSlotRig = itemSlotRig;

        _root = root;

        unitBodyAnimator.AnimatorItemUsagePhase.Subscribe(itemUsagePhase =>
        {
            //TODO: move to service that handles different items usages
            //Effect on range draw / release
            if (_itemSlotRig.PrimaryHandWeapon.Value is RangedWeapon rangedWeapon)
            {

                rangedWeapon.Use(itemUsagePhase, combatGroup);
            }
        });

        unitBodyAnimator.HasItemEquipped.Subscribe(BodyAnimatorHasWeaponEquipped);
    }

    private void BodyAnimatorHasWeaponEquipped(bool isItemEquipped)
    {
        if (isItemEquipped)
        {
            if (_itemSlotRig.PrimaryHandWeapon.Value) return;

            var primaryWeaponSlot = _itemSlotRig.GetSlot(ItemSlotUsage.PrimaryWeapon);

            _itemSlotRig.PrimaryHandWeapon.Value = primaryWeaponSlot.GetItemInstanceTyped();
            //TODO: Secondary hand
        }
        else
        {
            if (!_itemSlotRig.PrimaryHandWeapon.Value) return;

            _itemSlotRig.PrimaryHandWeapon.Value = null;

            //TODO: Secondary hand
        }
    }

    public void Dispose()
    {
    }
}