﻿using Zenject;

public class InstanceContextMonoInstaller : MonoInstaller<InstanceContextMonoInstaller>
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<InstanceContext>().AsSingle();
    }
}