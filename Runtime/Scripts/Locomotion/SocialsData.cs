﻿using System;

[Serializable]
public class SocialsData
{
    public SocialsType type;
    public string clipName;
    public float duration;
}