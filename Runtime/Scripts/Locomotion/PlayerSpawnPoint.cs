﻿using UnityEngine;
using Zenject;

public class PlayerSpawnPoint : MonoBehaviour
{
    public bool autoSpawnOnStart;

    private PlayerManager _playerManager;

    [Inject]
    private void Ctor(PlayerManager manager)
    {
        _playerManager = manager;
        manager.PlayerSpawnPoint = this;
    }

    private void Start()
    {
        if (autoSpawnOnStart)
        {
            _playerManager.SpawnBody();
        }
    }
}