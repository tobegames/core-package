﻿using Tobe.Core.Mvc.Implementations;
using UnityEngine;

public class ConfirmableUiWindow :
    MonoViewControllerProxy<ConfirmableWindowViewModel, ConfirmableWindowView, IConfirmableUiWindowController>,
    IConfirmableUiWindowController
{
    [SerializeField] private ConfirmableWindowView view;

    protected override ConfirmableWindowView View => view;
    
    public void Deactivate()
    {
        Controller.Deactivate();
    }

    
    public void Activate()
    {
        Controller.Activate();
    }
}