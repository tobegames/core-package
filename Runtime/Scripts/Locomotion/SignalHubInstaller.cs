﻿using Zenject;

public class SignalHubInstaller : Installer<SignalHubInstaller>
{
    public override void InstallBindings()
    {

        SignalBusInstaller.Install(Container);
        
        // Container.BindFactory<SignalHub, SignalHub.Factory>();
    }
}