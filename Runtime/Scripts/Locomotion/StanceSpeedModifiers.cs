﻿using System;

[Serializable]
public class StanceSpeedModifiers
{
    public float forwardSpeedMultiplier = 1;
    public float backwardSpeedMultiplier = 1;
    public float forwardDiagonalCoef = 1.2f;
    public float backwarddDiagonalCoef = 0.4f;
    public float strafeSpeed = 1;
}