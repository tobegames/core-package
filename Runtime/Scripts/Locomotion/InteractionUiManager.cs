﻿public class InteractionUiManager : IInteractionUiManager
{
    public InteractionUiManager(IConfirmableUiWindowController interactionPromptWindowController, IDialogWindowController dialogWindowController, IConfirmableUiWindowController achievementWindow)
    {
        InteractionPromptWindowController = interactionPromptWindowController;
        DialogWindowController = dialogWindowController;
        AchievementWindow = achievementWindow;
    }

    
    
    public IConfirmableUiWindowController InteractionPromptWindowController { get; }
    public IConfirmableUiWindowController AchievementWindow { get; }
    public IDialogWindowController DialogWindowController { get; }
}