﻿using UnityEngine;
using Zenject;

public class SpawnerMonoInstaller : MonoInstaller<SpawnerMonoInstaller>
{
    public GameObject prefab;

    public int poolInitialSize = 1;
    public override void InstallBindings()
    {
        Container.BindFactory<UnitMonoProxySpawnArgs, UnitMonoProxy, UnitMonoProxy.Factory>()
            .FromMonoPoolableMemoryPool(x => x.WithInitialSize(poolInitialSize).FromComponentInNewPrefab(prefab));

    }
}