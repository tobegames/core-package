﻿using UnityEngine;

[CreateAssetMenu(menuName = "Repositories/Projectile Repository")]
public class ProjectileRepository : ScriptableObject
{
    public GameObject[] items;
}