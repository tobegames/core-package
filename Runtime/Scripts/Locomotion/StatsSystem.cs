﻿using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Zenject;

public interface IStatsProvider
{
    FloatStat[]  Stats { get; }
    FloatStat LifeStat { get; }
}

public class NpcStatsProvider : IStatsProvider
{
    public NpcStatsProvider(NpcContainerContext context)
    {
        Stats = context.npcMetadata.stats;
        LifeStat = context.npcMetadata.lifeStat;
    }

    public FloatStat[] Stats { get; }
    public FloatStat LifeStat { get; }
}

public class PlayerStatsProvider : IStatsProvider
{
    public PlayerStatsProvider(CharacterClassSheet characterClassSheet)
    {
        Stats = characterClassSheet.stats;
        LifeStat = characterClassSheet.lifeStat;
    }

    public FloatStat[] Stats { get; }
    public FloatStat LifeStat { get; }
}

public class StatsSystem
{
    private FloatStat[] _stats;

    private readonly FloatStat _lifeStat;

    public IReadOnlyReactiveProperty<float> CurrentLife => _currentLife;

    private readonly FloatReactiveProperty _currentLife;

    public IDictionary<string, FloatReactiveProperty> Stats => _reactiveStats;

    private readonly IDictionary<string, FloatReactiveProperty> _reactiveStats;
    
    public StatsSystem(IStatsProvider characterClassSheet, SignalBus signalBus, IUnitIdentifier unitIdentifier, UnitBodyAnimator bodyAnimator)
    {
        _reactiveStats =
            characterClassSheet.Stats.ToDictionary(x => x.Id, stat => new FloatReactiveProperty(stat.CurrentValue));

        _lifeStat = characterClassSheet.LifeStat;

        _currentLife = new FloatReactiveProperty(_lifeStat.CurrentValue);
        
        _stats = characterClassSheet.Stats;

        
        signalBus.Subscribe<DamageSignal>(x =>
        {
            ChangeCurrentLife(-x.DamageAmount);
            
            if (CurrentLife.Value <= 0)
            {
                signalBus.TryFire(new QuestKillProgressSignal
                {
                    Amount = 1,
                    TargetId = unitIdentifier.UnitId
                });
            }
            
            bodyAnimator.Death();
        });
    }

    private void ChangeCurrentLife(float amount)
    {
        var value = Mathf.Clamp(CurrentLife.Value + amount, _lifeStat.MinValue, _lifeStat.MaxValue);
        
        _currentLife.Value = value;
    }
}