﻿public class SpellDamageSource : IDamageSource
{
    public SpellDamageSource(IUnitIdentifier unitIdentifier)
    {
        UnitIdentifier = unitIdentifier;
    }

    public IUnitIdentifier UnitIdentifier { get; }
}