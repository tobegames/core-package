﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

public class Spawner : MonoBehaviour
{
    [SerializeField] private int autoSpawnedUnits = 3;

    private UnitMonoProxy.Factory _factory;

    private List<UnitMonoProxy> _pool;

    [Inject]
    private void Ctor(UnitMonoProxy.Factory factory)
    {
        _factory = factory;
        _pool = new List<UnitMonoProxy>();

        for (int i = 0; i < autoSpawnedUnits; i++)
        {
            Spawn();
        }
    }

    [Button]
    public UnitMonoProxy Spawn()
    {
        var unit = _factory.Create(new UnitMonoProxySpawnArgs());
        if (!_pool.Contains(unit))
        {
            _pool.Add(unit);
        }

        return unit;
    }


    public void DespawnAll()
    {
        foreach (var unitMonoProxy in _pool)
        {
            unitMonoProxy.Dispose();
        }
        _pool.Clear();
    }

}