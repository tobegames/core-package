﻿using System;
using Sirenix.OdinInspector;
using Tobe.Core.Mvc;
using UnityEngine;

[Serializable]
public class UnitBodyAnimatorLayers
{
    [SerializeField] private string socialsLayerName = "Socials";

    public int SocialsLayerIndex { get; private set; }


    [SerializeField] private string combatLayerName = "Upper Body Action";

    public int CombatLayerIndex { get; private set; }

    public void Initialize(IAnimatorComponent monoAnimator)
    {
        SocialsLayerIndex = monoAnimator.GetLayerIndex(socialsLayerName);
        CombatLayerIndex = monoAnimator.GetLayerIndex(combatLayerName);
    }
}