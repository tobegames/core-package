﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;
using Zenject;
//
// public class PlayerCommander : MonoBehaviour
// {
//     private UnitBodyState _unitBodyState;
//
//     private PlayerManager _playerManager;
//
//     [Inject]
//     private void Ctor(PlayerManager playerManager)
//     {
//         _playerManager = playerManager;
//     }
//
//     private void Start()
//     {
//         _unitBodyState = GetComponent<UnitBodyState>();
//         
//         if (!_unitBodyState)
//             _unitBodyState = _playerManager.Instance;
//     }
//
//     private void Update()
//     {
//         if (!_unitBodyState) return;
//
//         _unitBodyState.Strafe(Input.GetAxis("Horizontal"));
//
//         _unitBodyState.Push(Input.GetAxis("Vertical"));
//
//         if (Input.GetKeyDown(KeyCode.Space))
//         {
//             if (_unitBodyState.CurrentWeapon != null)
//             {
//                 if (_unitBodyState.CurrentWeapon.IsMelee)
//                 {
//                     _unitBodyState.MeleeAttack();
//                 }
//                 else
//                 {
//                     _unitBodyState.RangedAttackPrepare();
//                     _unitBodyState.RangedAttackUnleash();
//                 }
//             }
//         }
//
//         if (Input.GetKeyUp(KeyCode.Space))
//         {
//             _unitBodyState.RangedAttackUnleash();
//         }
//
//         if (Input.GetKeyDown(KeyCode.LeftShift))
//         {
//             _unitBodyState.EnterRun();
//         }
//         else if (Input.GetKeyUp(KeyCode.LeftShift))
//         {
//             _unitBodyState.EnterWalk();
//         }
//
//         if (Input.GetKeyDown(KeyCode.Tab))
//         {
//             if (!_unitBodyState.IsInCombat)
//             {
//                 _unitBodyState.Arm(_unitBodyState.itemSlots.meleeWeaponSlot);
//                 _unitBodyState.EnterCombat();
//             }
//             else
//             {
//                 _unitBodyState.DisarmWeapon();
//                 _unitBodyState.EnterWalk();
//             }
//         }
//
//         if (EventSystem.current && Input.GetMouseButton((int) MouseButton.LeftMouse) &&
//             !EventSystem.current.currentSelectedGameObject)
//         {
//             var hittedCollider = GetScreenToWorldObject(LayerMask.NameToLayer("Default"), 100);
//             if (hittedCollider)
//             {
//                 var npc = hittedCollider.GetComponent<AiCommander>();
//                 if (npc)
//                     npc.BeginInteractWithPlayer();
//             }
//         }
//     }
//
//
//     public Collider GetScreenToWorldObject(LayerMask mask, float distance)
//     {
//         var mainCamera = Camera.main;
//
//         var position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, mainCamera.nearClipPlane);
//
//         var ray = mainCamera.ScreenPointToRay(position);
//
//         RaycastHit rayHit;
//
//         if (Physics.Raycast(ray, out rayHit, distance, mask))
//         {
//             return rayHit.collider;
//         }
//
//         return null;
//     }
//
//     public void Attack()
//     {
//         _unitBodyState.Arm(_unitBodyState.itemSlots.rangedWeaponSlot);
//
//         //  unitCommandExecutorProxy.MeleeAttack();
//     }
// }