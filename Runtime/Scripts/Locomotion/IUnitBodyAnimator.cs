﻿using System;
using UnityEngine;

public interface IUnitBodyAnimator : IDisposable
{
    void Block();
    void DisarmWeapon();
    void DrawWeapon();
}