﻿using UnityEngine;
using Zenject;

public class WeaponDrop : MonoBehaviour
{
    public Weapon itemPrefab;

    public Transform displayPoint;

    public bool isRotating;

    public float rotationMultiplier = 1f;

    private DiContainer _diContainer;


    [Inject]
    private void Ctor(DiContainer diContainer)
    {
        _diContainer = diContainer;
    }

    public void Start()
    {
        var instance =
            _diContainer.InstantiatePrefabForComponent<Weapon>(itemPrefab, displayPoint);

        instance.transform.localPosition = Vector3.zero;
    }

    private void Update()
    {
        if (isRotating)
            displayPoint.transform.rotation *= Quaternion.Euler(0, rotationMultiplier * Time.deltaTime, 0);
    }
}