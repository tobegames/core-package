﻿using System;
using UniRx;
using UnityEngine;
using Zenject;
using Object = System.Object;

public class UnitTransition : MonoBehaviour, IDisposable
{
    private Rigidbody _rigidbody;

    private bool _updateTransform = true;

    private Vector3 _velocityNormalized;

    private Transform _rootTransform;

    private UnitBodyAnimator _bodyAnimator;
    private LocomotionTweak _locomotionTweak;

    [Inject]
    public void Ctor(LocomotionTweak locomotionTweak,
        Transform rootTransform,
        Rigidbody rb,
        UnitBodyAnimator bodyAnimator,
        IPlayerInputs playerInputs
    )
    {
        playerInputs.Velocity.Subscribe(x => _velocityNormalized = x);

        _locomotionTweak = locomotionTweak;
        _rigidbody = rb;
        _bodyAnimator = bodyAnimator;
        _rootTransform = rootTransform;
    }


    public void Update()
    {
        if (!_bodyAnimator)
            return;


        if (_updateTransform)
        {
            var multiplier = _velocityNormalized.z > 0
                ? _locomotionTweak.forwardMultiplier
                : _locomotionTweak.backwardMultiplier;
            
            var rootTransformForward = _rigidbody.position +
                                       _rootTransform.forward *
                                       (_velocityNormalized.z * multiplier* Time.deltaTime);

            _rigidbody.MovePosition(rootTransformForward);

            _rigidbody.MoveRotation(_rigidbody.rotation *
                                    Quaternion.Euler(0,
                                        _velocityNormalized.x * Time.deltaTime * _locomotionTweak.angularSpeed,
                                        0));
        }
    }

    public void Dispose()
    {
        enabled = false;
    }
}