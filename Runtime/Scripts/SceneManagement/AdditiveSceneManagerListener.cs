﻿using System;
using UnityEngine;
using Zenject;

namespace Tobe.Core
{
    public class AdditiveSceneManagerListener : MonoBehaviour
    {
        public AdditiveSceneManagerEvent onGroupLoading;

        [Inject] private IAdditiveSceneManager _manager;

        public bool loadDefaultGroupOnStart = true;

        private void Start()
        {
            _manager.LoadingGroup += ManagerOnLoadingGroup;

            if (loadDefaultGroupOnStart)
                _manager.LoadDefaultGroup();
        }

        private void OnDestroy()
        {
            _manager.LoadingGroup -= ManagerOnLoadingGroup;
        }

        private void ManagerOnLoadingGroup(object sender, AdditiveSceneLoadGroup e)
        {
            onGroupLoading?.Invoke(e);
        }
    }
}