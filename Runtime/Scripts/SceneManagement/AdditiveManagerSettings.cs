﻿using Tobe.Core.Mvc.Implementations;
using UnityEngine;

namespace Tobe.Core
{
    [CreateAssetMenu(fileName = nameof(AdditiveManagerSettings),
        menuName = "Scene Manager/" + nameof(AdditiveManagerSettings))]
    public class AdditiveManagerSettings : ScriptableObject
    {
        [Scene]
        public string transitionScene;
        
        public AdditiveSceneLoadGroup defaultGroup;
    }
    
    
}