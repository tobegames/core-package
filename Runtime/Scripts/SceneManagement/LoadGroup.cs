﻿using System;

namespace Tobe.Core
{
    [Flags]
    public enum LoadGroup
    {
        None = 0,
        ReloadIfSame = 1
    }
}