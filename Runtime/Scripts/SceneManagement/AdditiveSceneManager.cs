﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Tobe.Core.Mvc;
using Tobe.Core.Mvc.Implementations;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Tobe.Core
{
//    public static class FlagHelper
//    {
//        public static bool HasFlag<TFlag>(this TFlag instance, TFlag check) where TFlag : class, FlagsAttribute
//        {
//            return (instance & check) != 0;
//        }
//    }


    public class AdditiveSceneManager : IAdditiveSceneManager, IInitializable, IDisposable
    {
        public bool HasDefaultGroupLoaded { get; private set; }

        private readonly List<string> _loadedScenes;

        public event EventHandler<AdditiveSceneLoadGroup> LoadingGroup;

        private AdditiveManagerSettings _settings;

        private AdditiveSceneLoadGroup _currentGroup;

        private readonly ICoroutineProcessor _coroutineProcessor;

        public IReadOnlyList<string> LoadedScenes => _loadedScenes;


        public AdditiveSceneManager(ICoroutineProcessor coroutineProcessor)
        {
            _coroutineProcessor = coroutineProcessor;

            _loadedScenes = new List<string>();

            var sceneCount = SceneManager.sceneCount;

            for (int i = 0; i < sceneCount; i++)
            {
                var loadedScene = SceneManager.GetSceneAt(i);

                _loadedScenes.Add(loadedScene.name);
            }

        }

        public void LoadGroup(AdditiveSceneLoadGroup loadGroup,
            UnloadSceneOptions unloadSceneOptions = UnloadSceneOptions.None, LoadGroup options = Core.LoadGroup.None)
        {
            if (_currentGroup == loadGroup && !options.HasFlag(Core.LoadGroup.ReloadIfSame)) return;

            _currentGroup = loadGroup;

            _coroutineProcessor.Build(LoadCurrentGroup()).Execute();
        }

        private IEnumerator LoadCurrentGroup()
        {
            // Check if transition scene is loaded
            var isTransitionSceneLoaded = _loadedScenes.Any(x =>
                x.Equals(_settings.transitionScene, StringComparison.InvariantCultureIgnoreCase));

            if (!isTransitionSceneLoaded)
            {
                // Load transition scene
                yield return SceneManager.LoadSceneAsync(_settings.transitionScene);

                _loadedScenes.Add(_settings.transitionScene);

                isTransitionSceneLoaded = true;
            }

            yield return new WaitForSecondsRealtime(1);

            OnLoadingGroup(_currentGroup);

            _loadedScenes.Clear();


            var decorators = _currentGroup.decoratingScenes;

            var main = _currentGroup.mainScene;

            foreach (var decorator in decorators)
            {
                var load = SceneManager.LoadSceneAsync(decorator, LoadSceneMode.Additive);

                yield return load;
                _loadedScenes.Add(decorator);
            }

            var operation = SceneManager.LoadSceneAsync(main, LoadSceneMode.Additive);

            yield return operation;

            if (isTransitionSceneLoaded)
                yield return SceneManager.UnloadSceneAsync(_settings.transitionScene);

            _loadedScenes.Add(main);
        }

        protected virtual void OnLoadingGroup(AdditiveSceneLoadGroup e)
        {
            LoadingGroup?.Invoke(this, e);
        }


        public void Initialize()
        {
            _settings = Resources.Load<AdditiveManagerSettings>(nameof(AdditiveManagerSettings));
            
            var loadedSceneName = SceneManager.GetActiveScene().name;
            
            if (!string.IsNullOrEmpty(loadedSceneName) && loadedSceneName.Equals(_settings.defaultGroup.mainScene))
            {
                HasDefaultGroupLoaded = true;
            }
            MonoBehaviour.print("Current scene is = " + SceneManager.GetActiveScene().name);

        }


        public void LoadDefaultGroup()
        {
            if (HasDefaultGroupLoaded) return;

            // This is called only on app init

            LoadGroup(_settings.defaultGroup);

            HasDefaultGroupLoaded = true;
        }

        public void Dispose()
        {
            if (_settings)
                Resources.UnloadAsset(_settings);
        }
    }
}