﻿using UnityEngine;
using Zenject;

namespace Tobe.Core
{
    public class AdditiveSceneManagerProxy : MonoBehaviour
    {
        private IAdditiveSceneManager _additiveScene;


        [Inject]
        public void Ctor(IAdditiveSceneManager additiveSceneManager)
        {
            _additiveScene = additiveSceneManager;
        }

        public void LoadGroup(AdditiveSceneLoadGroup @group)
        {
            _additiveScene.LoadGroup(group);
        }
    }
}