﻿using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace Tobe.Core
{
    public interface IAdditiveSceneManager
    {
        event EventHandler<AdditiveSceneLoadGroup> LoadingGroup;

        void LoadDefaultGroup();

        IReadOnlyList<string> LoadedScenes { get; }
        
        void LoadGroup(AdditiveSceneLoadGroup loadGroup,
            UnloadSceneOptions unloadSceneOptions = UnloadSceneOptions.None, LoadGroup options = Core.LoadGroup.None);
    }
}